/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.mobileclass.unipplus;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.mobileclass.unipplus";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 620;
  public static final String VERSION_NAME = "6.2.0";
}
