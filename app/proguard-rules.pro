# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Volumes/Aquivos/Android/android-sdk-macosx/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keepattributes Signature
-keepattributes *Annotation*

# Nescessario mander as classes que fazem parte da conversao do JSON
-keep class com.mobileclass.unipplus.modules.login.entity.UserEntity { *; }
-keep class com.mobileclass.unipplus.modules.login.entity.LoginResponseEntity { *; }
-keep class com.mobileclass.unipplus.modules.login.entity.SystemsEntity { *; }
-keep class com.mobileclass.unipplus.modules.home.entities.RateEntity { *; }


## OKHTTP3 CONFIGURATION
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**
-dontwarn okio.**

## GJSON CONFIGURATION
-dontwarn sun.misc.**
-keep class * extends com.google.gson.TypeAdapter
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer
-keepclassmembers,allowobfuscation class * {
  @com.google.gson.annotations.SerializedName <fields>;
}

## JSOUP CONFIGURATION
-keep public class org.jsoup.** {
    public *;
}
-keeppackagenames org.jsoup.nodes