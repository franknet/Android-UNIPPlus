package com.mobileclass.unipplus.modules.login;

import com.mobileclass.unipplus.modules.login.entity.LoginResponseEntity;
import com.mobileclass.unipplus.modules.login.entity.UserEntity;
import com.mobileclass.unipplus.modules.login.interactor.LoginInteractor;
import com.mobileclass.unipplus.modules.login.interactor.LoginInteractorCallback;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LoginInteractorTest implements LoginInteractorCallback {
    private LoginInteractor mInteractor;

    @Before
    public void setUp() {
        mInteractor = new LoginInteractor();
        mInteractor.setCallback(this);
    }

    @After
    public void tearDown() {
        mInteractor = null;
    }

    @Test
    public void testLoginAuthentication() {
        UserEntity entity = new UserEntity();
        mInteractor.doLogin(entity);
        Assert.assertTrue(true);
    }

    @Override
    public void onLoginAuthenticatedWithSuccess(LoginResponseEntity result) {

    }

    @Override
    public void onLoginAuthenticatedWithFailure(String message) {

    }

    @Override
    public void onSessionInitializedWithSuccess() {

    }

    @Override
    public void onSessionInitializedWithFailure(String message) {

    }

    @Override
    public void onVerifyNewVersion(boolean showNewVersionAlert) {

    }
}
