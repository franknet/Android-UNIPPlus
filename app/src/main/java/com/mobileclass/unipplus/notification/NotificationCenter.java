package com.mobileclass.unipplus.notification;

public class NotificationCenter {
    private static final NotificationCenter ourInstance = new NotificationCenter();
    private NotificationCenterListener listener;

    public static NotificationCenter build() {
        return ourInstance;
    }

    private NotificationCenter() {
    }

    public void setNotificationListener(NotificationCenterListener listener) {
        this.listener = listener;
    }

    public void postNotification(int notificationId, Object object) {
        if (listener != null) {
            listener.notification(notificationId, object);
        }
    }

}
