package com.mobileclass.unipplus.notification;

public interface NotificationCenterListener {
    void notification(int notificationID, Object object);
}
