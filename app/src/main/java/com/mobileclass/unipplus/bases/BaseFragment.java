package com.mobileclass.unipplus.bases;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.mobileclass.unipplus.directory.SharedPrefDataAccess;
import com.mobileclass.unipplus.services.google.GoogleAnalytcsService;
import com.mobileclass.unipplus.utils.JSONUtils;

import java.util.HashMap;
import java.util.Map;

public class BaseFragment extends Fragment {
    private boolean runningInBackground;
    private Map<String, Object> objectStateCollecion = new HashMap<>();
    private GoogleAnalytcsService googleAnalytcsService;

    // PUBLIC METHODS
    public boolean isRunningInBackground() {
        return runningInBackground;
    }

    public void setRunningInBackground(boolean runningInBackground) {
        this.runningInBackground = runningInBackground;
    }

    public Object getObjectState(String key) {
        return this.objectStateCollecion.get(key);
    }

    public void setObjectState(String key, Object objectState) {
        this.objectStateCollecion.put(key, objectState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.googleAnalytcsService = new GoogleAnalytcsService((AppCompatActivity) this.getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        this.setRunningInBackground(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        this.setRunningInBackground(true);
    }

    @Override
    public void onStop() {
        super.onStop();
        this.setRunningInBackground(true);
    }

    // PROTECTED METHODS
    protected void sendScreenName(String screenName) {
        if (this.googleAnalytcsService != null) {
            this.googleAnalytcsService.sendScreenName(screenName);
        }
    }

    protected void saveData(Object data, String key) {
        String jsonData = JSONUtils.serialize(data);
        new SharedPrefDataAccess(getContext()).saveData(key, jsonData);
    }

    protected Object getSavedData(String key, Class objectClass) {
        String jsonString = new SharedPrefDataAccess(getContext()).getData(key);
        return JSONUtils.deserialize(jsonString, objectClass);
    }
}
