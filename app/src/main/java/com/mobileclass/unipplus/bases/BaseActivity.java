package com.mobileclass.unipplus.bases;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AppCompatActivity;

import com.mobileclass.unipplus.R;
import com.mobileclass.unipplus.directory.SharedPrefDataAccess;
import com.mobileclass.unipplus.services.google.GoogleAnalytcsService;
import com.mobileclass.unipplus.utils.JSONUtils;

public class BaseActivity extends AppCompatActivity {
    protected boolean isSmartphone;
    protected GoogleAnalytcsService googleAnalytcsService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        googleAnalytcsService = new GoogleAnalytcsService(this);

        this.isSmartphone = getResources().getBoolean(R.bool.is_smartphone);

        if (this.isSmartphone) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    protected void dismissKeyBoard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        if (getCurrentFocus() != null) {
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    protected void sendScreenName(String screenName) {
        if (this.googleAnalytcsService != null) {
            this.googleAnalytcsService.sendScreenName(screenName);
        }
    }

    protected void saveData(Object data, String key) {
        String jsonString = JSONUtils.serialize(data);
        new SharedPrefDataAccess(this).saveData(key, jsonString);
    }

    protected Object getSavedData(String key, Class objectClass) {
        String jsonString = new SharedPrefDataAccess(this).getData(key);
        return JSONUtils.deserialize(jsonString, objectClass);
    }
}
