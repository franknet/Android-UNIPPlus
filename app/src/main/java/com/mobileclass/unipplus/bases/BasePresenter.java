package com.mobileclass.unipplus.bases;

public interface BasePresenter {
    void start();
}
