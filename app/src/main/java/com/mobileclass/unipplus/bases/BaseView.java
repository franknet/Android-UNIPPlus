package com.mobileclass.unipplus.bases;

/**
 * Created by Jose Franklin on 28/07/2017.
 */

public interface BaseView {
    void showLoading(boolean show);
    void hideKeyboard();
    void showAlertDialog(String title, String message);
}
