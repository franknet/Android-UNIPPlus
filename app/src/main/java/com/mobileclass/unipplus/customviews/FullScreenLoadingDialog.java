package com.mobileclass.unipplus.customviews;

import android.app.Dialog;
import android.content.Context;

import com.mobileclass.unipplus.R;

public class FullScreenLoadingDialog extends Dialog {
    public FullScreenLoadingDialog(Context context) {
        super(context, android.R.style.Theme_Light_NoTitleBar_Fullscreen);

        setContentView(R.layout.dialog_login_loading);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setTitle(null);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void dismiss() {
        if (this.isShowing()) {
            super.dismiss();
        }
    }
}
