package com.mobileclass.unipplus.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.mobileclass.unipplus.R;

public class ResizeMenuLayout extends RelativeLayout {
    private FrameLayout menuLeftContainer;
    private CardView contentContainer;
    private FragmentManager manager;
    private boolean flagIsOpen = false;

    public ResizeMenuLayout(Context context, FragmentManager fragmentManager) {
        super(context);
        this.manager = fragmentManager;
        setUp();
    }

    public ResizeMenuLayout(Context context) {
        super(context);
        setUp();
    }

    public ResizeMenuLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        setUp();
    }

    public void openLeftMenu() {
        this.flagIsOpen = false;
        animateContentContainer();
    }

    public void closeLeftMenu() {
        this.flagIsOpen = true;
        animateContentContainer();
    }

    public boolean isOpen() {
        return  flagIsOpen;
    }

    public void setContentMenu(Fragment fragment) {
        addFragmentToContainer(fragment, this.contentContainer);
    }

    public void setMenuLeft(Fragment fragment) {
        addFragmentToContainer(fragment, this.menuLeftContainer);
    }

    private void setUp() {
        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        this.menuLeftContainer = new FrameLayout(getContext());
        this.menuLeftContainer.setId(R.id.residemenu_layout_menuleft_id);
        this.menuLeftContainer.setLayoutParams(params);
        addView(this.menuLeftContainer);

        this.contentContainer = new CardView(getContext());
        this.contentContainer.setId(R.id.residemenu_layout_content_id);
        this.contentContainer.setLayoutParams(params);
        this.contentContainer.setFocusableInTouchMode(true);
        this.contentContainer.setClickable(true);
        this.contentContainer.setCardElevation(getResources().getDimensionPixelOffset(R.dimen.size_6));
        addView(this.contentContainer);
    }

    private void animateContentContainer() {
        float x = 0, y = 0, scaleX = 1.0f, scaleY = 1.0f;

        if (!this.flagIsOpen) {
            x = (getResources().getInteger(R.integer.side_menu_move_range) * getWidth() / 100);
            y = 0;
            scaleX = 0.7f;
            scaleY = 0.7f;
            this.flagIsOpen = true;
        } else {
            this.flagIsOpen = false;
        }

        this.contentContainer.animate().x(x).y(y).scaleX(scaleX).scaleY(scaleY).setDuration(300).start();
    }

    private void addFragmentToContainer(Fragment fragment, ViewGroup container) {
        FragmentTransaction transaction = this.manager.beginTransaction();
        transaction.replace(container.getId(), fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
