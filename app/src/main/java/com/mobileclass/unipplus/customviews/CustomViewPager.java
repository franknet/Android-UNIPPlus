package com.mobileclass.unipplus.customviews;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.mobileclass.unipplus.R;

public class CustomViewPager extends RelativeLayout {

    public interface CustomViewPagerListener {
        void onPageSelected(int position);
    }

    private CustomViewPagerListener listener;
    private ViewPager viewPager;
    private LinearLayout pagerIndicator;
    private int itemsCount = 0;

    //PUBLIC METHODS
    public CustomViewPager(Context context) {
        super(context);
        setUp();
    }

    public void setListener(CustomViewPagerListener listener) {
        this.listener = listener;
    }

    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        setUp();
    }

    public void setAdapter(PagerAdapter adapter) {
        this.itemsCount = adapter.getCount();
        this.viewPager.setAdapter(adapter);
        this.viewPager.setOffscreenPageLimit(itemsCount);
        setPageIndicatorCount();
    }

    public void setCurrentItem(int item, boolean smoothScroll) {
        this.viewPager.setCurrentItem(item, smoothScroll);
        setPageSelectedAtPosition(item);
    }

    //PRIVATE METHODS
    private void setUp() {
        createViewPagerInidicator();
        createViewPager();
    }

    private void createViewPager() {
        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(ALIGN_PARENT_TOP);
        params.addRule(ALIGN_PARENT_RIGHT);
        params.addRule(ALIGN_PARENT_LEFT);
        params.addRule(ABOVE, this.pagerIndicator.getId());

        this.viewPager = new ViewPager(getContext());
        this.viewPager.setId(R.id.custom_view_pager_indicator_id);
        this.viewPager.setLayoutParams(params);
        this.viewPager.setOnPageChangeListener(onPageChangeListener());

        addView(this.viewPager);
    }

    private void createViewPagerInidicator() {
        int padding = getResources().getDimensionPixelOffset(R.dimen.size_12);

        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(ALIGN_PARENT_BOTTOM);
        params.addRule(ALIGN_PARENT_RIGHT);
        params.addRule(ALIGN_PARENT_LEFT);

        this.pagerIndicator = new LinearLayout(getContext());
        this.pagerIndicator.setId(R.id.custom_view_pager_id);
        this.pagerIndicator.setOrientation(LinearLayout.HORIZONTAL);
        this.pagerIndicator.setLayoutParams(params);
        this.pagerIndicator.setPadding(padding, padding, 0, padding);
        this.pagerIndicator.setGravity(Gravity.CENTER);
        this.pagerIndicator.setBackgroundColor(getResources().getColor(R.color.color_007));

        addView(this.pagerIndicator);
    }

    private View createPageIndicatorDot() {
        int dotSize = getResources().getDimensionPixelOffset(R.dimen.size_8);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(dotSize, dotSize);
        params.setMargins(0, 0, getResources().getDimensionPixelOffset(R.dimen.size_12), 0);

        View dotView = new View(getContext());
        dotView.setLayoutParams(params);

        return dotView;
    }

    private void setPageIndicatorCount() {
        this.pagerIndicator.removeAllViews();

        for (int i = 0; i < this.itemsCount; i++) {
            this.pagerIndicator.addView(createPageIndicatorDot());
        }
    }

    private void setPageSelectedAtPosition(int position) {
        int pageIndicatorSelectedColor = R.color.color_001;
        int pageIndicatorUnselectedColor = R.color.color_006;

        for (int i = 0; i < this.pagerIndicator.getChildCount(); i++) {
            View child = this.pagerIndicator.getChildAt(i);

            GradientDrawable background = new GradientDrawable();
            background.setShape(GradientDrawable.OVAL);

            if (i == position) {
                background.setColor(getResources().getColor(pageIndicatorSelectedColor));
            } else {
                background.setColor(getResources().getColor(pageIndicatorUnselectedColor));
            }

            child.setBackgroundDrawable(background);
        }
    }

    //VIEW PAGER EVENTS
    private ViewPager.SimpleOnPageChangeListener onPageChangeListener() {
        return new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                setPageSelectedAtPosition(position);

                if (listener != null) {
                    listener.onPageSelected(position);
                }
            }
        };
    }
}
