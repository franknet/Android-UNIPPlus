package com.mobileclass.unipplus;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.firebase.FirebaseApp;
import com.mobileclass.unipplus.services.google.RemoteConfig;

import java.io.IOException;

public class UNIPPlusApplication extends Application {
    private static UNIPPlusApplication mApplication;
    private UNIPPlusApplicationCache mCache;
    private UNIPPlusApplicationInfo mInfo;
    private UNIPPlusApplicationTemp mTemp;

    public static UNIPPlusApplication getInstance() {
        return mApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mApplication = this;
        mCache = new UNIPPlusApplicationCache(this);
        mInfo = new UNIPPlusApplicationInfo(this);
        mTemp = new UNIPPlusApplicationTemp();

        FirebaseApp.initializeApp(this);
        RemoteConfig.shared().initialize();
    }

    public void hasInternetConnection() throws IOException {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        if (netInfo == null || !netInfo.isConnected()) {
            throw new IOException("Não há conexão com a internet!");
        }
    }

    public UNIPPlusApplicationTemp getTemp() {
        return mTemp;
    }

    public UNIPPlusApplicationCache getCache() {
        return mCache;
    }

    public UNIPPlusApplicationInfo getInfo() {
        return mInfo;
    }
}
