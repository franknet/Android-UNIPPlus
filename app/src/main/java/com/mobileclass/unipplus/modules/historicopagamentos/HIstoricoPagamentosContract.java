package com.mobileclass.unipplus.modules.historicopagamentos;

import com.mobileclass.unipplus.bases.BasePresenter;
import com.mobileclass.unipplus.bases.BaseView;
import com.mobileclass.unipplus.modules.historicopagamentos.entities.HistoricoPagamentosPosicaoEntity;

import java.util.List;

public interface HIstoricoPagamentosContract {
    interface View extends BaseView {
        void setTotalBills(String total);
        void setTableViewData(List<List<HistoricoPagamentosPosicaoEntity>> groupedHistoricoPagamentos);
    }

    interface Presenter extends BasePresenter {

    }
}
