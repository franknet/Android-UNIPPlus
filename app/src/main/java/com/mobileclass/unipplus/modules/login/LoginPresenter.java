package com.mobileclass.unipplus.modules.login;

import com.mobileclass.unipplus.UNIPPlusApplication;
import com.mobileclass.unipplus.modules.login.entity.LoginResponseEntity;
import com.mobileclass.unipplus.modules.login.entity.UserEntity;
import com.mobileclass.unipplus.modules.login.interactor.LoginInteractor;
import com.mobileclass.unipplus.modules.login.interactor.LoginInteractorCallback;

public class LoginPresenter implements LoginContract.Presenter, LoginInteractorCallback {
    private final LoginContract.View mView;
    private final LoginInteractor mInteractor;
    private LoginResponseEntity mResult;

    //PUBLIC METHODS
    LoginPresenter(LoginContract.View view) {
        mView = view;
        mInteractor = new LoginInteractor();
        mInteractor.setCallback(this);
    }

    @Override
    public void start() {
        UserEntity userEntity = UNIPPlusApplication.getInstance().getCache().getUserEntity();
        if (userEntity != null && userEntity.isKeepCredentials()) {
            mView.setRA(userEntity.getRa());
            mView.setPassword(userEntity.isKeepCredentials() ? userEntity.getPassword() : "");
            mView.checkKeepCredentials(userEntity.isKeepCredentials());
        }
        mInteractor.verifyNewVersion();
    }

    @Override
    public void onEntrarButtonClicked(UserEntity userEntity) {
        mView.showLoading(true);
        mInteractor.doLogin(userEntity);
        UNIPPlusApplication.getInstance().getCache().setUserEntity(userEntity);
    }

    //INTERACTOR CALLBACK METHODS
    @Override
    public void onLoginAuthenticatedWithSuccess(LoginResponseEntity result) {
        mResult = result;
        mInteractor.initSession(mResult.token);
    }

    @Override
    public void onLoginAuthenticatedWithFailure(String message) {
        mView.showLoading(false);
        mView.showAlertDialog("Atenção", message);
    }

    @Override
    public void onSessionInitializedWithSuccess() {
        mView.startHomeActivity(mResult);
    }

    @Override
    public void onSessionInitializedWithFailure(String message) {
        mView.showLoading(false);
        mView.showAlertDialog("Atenção", message);
    }

    @Override
    public void onVerifyNewVersion(boolean showNewVersionAlert) {
        if (showNewVersionAlert) {
            mView.showNewVersionAlert();
        }
    }
}
