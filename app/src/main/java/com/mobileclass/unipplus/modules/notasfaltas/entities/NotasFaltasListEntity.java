package com.mobileclass.unipplus.modules.notasfaltas.entities;

import java.util.ArrayList;

public class NotasFaltasListEntity {
    private ArrayList<NotasFaltasDisciplinaEntity> mDisciplinaEntities;

    public ArrayList<NotasFaltasDisciplinaEntity> getDisciplinas() {
        return mDisciplinaEntities;
    }

    public void setDisciplinas(ArrayList<NotasFaltasDisciplinaEntity> disciplinas) {
        mDisciplinaEntities = disciplinas;
    }
}
