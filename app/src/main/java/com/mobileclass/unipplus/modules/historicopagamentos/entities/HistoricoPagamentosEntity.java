package com.mobileclass.unipplus.modules.historicopagamentos.entities;

import java.util.List;

public class HistoricoPagamentosEntity {
    private List<List<HistoricoPagamentosPosicaoEntity>> groupedHistoricoPagamentos;

    public List<List<HistoricoPagamentosPosicaoEntity>> getGroupedHistoricoPagamentos() {
        return groupedHistoricoPagamentos;
    }

    public void setGroupedHistoricoPagamentos(List<List<HistoricoPagamentosPosicaoEntity>> groupedHistoricoPagamentos) {
        this.groupedHistoricoPagamentos = groupedHistoricoPagamentos;
    }
}
