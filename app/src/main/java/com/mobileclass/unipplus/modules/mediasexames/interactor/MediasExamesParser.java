package com.mobileclass.unipplus.modules.mediasexames.interactor;

import com.mobileclass.unipplus.modules.mediasexames.entities.MediasExamesDisciplinaEntity;
import com.mobileclass.unipplus.modules.mediasexames.entities.MediasExamesListEntity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class MediasExamesParser {
    static MediasExamesListEntity parse(byte[] data) {

        try {
            MediasExamesListEntity mediasExamesListEntity = new MediasExamesListEntity();
            ArrayList<MediasExamesDisciplinaEntity> disciplinas = new ArrayList<>();
            String html = new String(data);
            Document document = Jsoup.parse(html);
            Element table = document.getElementsByClass("table-striped").first();
            Elements trs = table.getElementsByTag("tr");
            trs.remove(0);
            Comparator<Element> comparator = new Comparator<Element>() {
                @Override
                public int compare(Element lhs, Element rhs) {
                    String type = lhs.getElementsByTag("td").get(3).text();
                    String compareType = rhs.getElementsByTag("td").get(3).text();
                    return type.compareTo(compareType);
                }
            };
            Collections.sort(trs, Collections.reverseOrder(comparator));
            for (Element tr : trs) {
                MediasExamesDisciplinaEntity disciplinaEntity = new MediasExamesDisciplinaEntity();
                String tdValue = tr.getElementsByTag("td").get(0).text().trim();
                disciplinaEntity.setDisciplina(tdValue);
                tdValue = tr.getElementsByTag("td").get(1).text().trim();
                disciplinaEntity.setNomedisciplina(tdValue);
                tdValue = tr.getElementsByTag("td").get(2).text().trim();
                disciplinaEntity.setTurmaespecial(tdValue);
                tdValue = tr.getElementsByTag("td").get(3).text().trim();
                disciplinaEntity.setTipodisciplina(tdValue);
                tdValue = tr.getElementsByTag("td").get(4).text().trim();
                disciplinaEntity.setObsdisciplina(tdValue);
                tdValue = tr.getElementsByTag("td").get(5).text().trim();
                disciplinaEntity.setFaltas(tdValue);
                tdValue = tr.getElementsByTag("td").get(6).text().trim();
                disciplinaEntity.setMediasemestral(tdValue);
                tdValue = tr.getElementsByTag("td").get(7).text().trim();
                disciplinaEntity.setExame(tdValue);
                tdValue = tr.getElementsByTag("td").get(8).text().trim();
                disciplinaEntity.setMediafinal(tdValue);
                disciplinas.add(disciplinaEntity);
            }
            mediasExamesListEntity.setDisciplinas(disciplinas);
            return mediasExamesListEntity;
        } catch (Exception e) {
            return null;
        }
    }
}
