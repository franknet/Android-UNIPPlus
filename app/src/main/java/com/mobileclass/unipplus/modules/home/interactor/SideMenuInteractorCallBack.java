package com.mobileclass.unipplus.modules.home.interactor;

import android.graphics.Bitmap;

import com.mobileclass.unipplus.modules.home.entities.SideMenuListEntity;

import java.util.ArrayList;

public interface SideMenuInteractorCallBack {
    void onContentsLoaded(ArrayList<SideMenuListEntity> sideMenuListEntities);
    void onUserImageLoaded(Bitmap userImage);
}
