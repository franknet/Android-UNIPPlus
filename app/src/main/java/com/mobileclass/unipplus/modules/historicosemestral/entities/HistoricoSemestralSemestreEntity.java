package com.mobileclass.unipplus.modules.historicosemestral.entities;

import java.util.ArrayList;

public class HistoricoSemestralSemestreEntity {
    private ArrayList<HistoricoSemestralDisciplinaEntity> disciplina;
    private String semestre;
    private int aproveitamento;
    private int aproveitamentoColor;

    public ArrayList<HistoricoSemestralDisciplinaEntity> getDisciplinas() {
        return disciplina;
    }

    public void setDisciplina(ArrayList<HistoricoSemestralDisciplinaEntity> disciplina) {
        this.disciplina = disciplina;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getSemestre() {
        return semestre;
    }

    public int getAproveitamento() {
        return aproveitamento;
    }

    public void setAproveitamento(int aproveitamento) {
        this.aproveitamento = aproveitamento;
    }

    public int getAproveitamentoColor() {
        return aproveitamentoColor;
    }

    public void setAproveitamentoColor(int aproveitamentoColor) {
        this.aproveitamentoColor = aproveitamentoColor;
    }
}
