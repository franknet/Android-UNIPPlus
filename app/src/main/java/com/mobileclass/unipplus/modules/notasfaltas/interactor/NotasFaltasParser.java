package com.mobileclass.unipplus.modules.notasfaltas.interactor;

import com.mobileclass.unipplus.modules.notasfaltas.entities.NotasFaltasDisciplinaEntity;
import com.mobileclass.unipplus.modules.notasfaltas.entities.NotasFaltasListEntity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class NotasFaltasParser {
    static NotasFaltasListEntity parse(byte[] data) {
        try {
            NotasFaltasListEntity notasFaltasListEntity = new NotasFaltasListEntity();
            ArrayList<NotasFaltasDisciplinaEntity> disciplinaEntities = new ArrayList<>();
            String html = new String(data);
            Document document = Jsoup.parse(html);
            Element table = document.getElementsByClass("table-striped").first();
            Elements trs = table.getElementsByTag("tr");
            trs.remove(0);
            Comparator<Element> comparator = new Comparator<Element>() {
                @Override
                public int compare(Element lhs, Element rhs) {
                    String type = lhs.getElementsByTag("td").get(3).text();
                    String compareType = rhs.getElementsByTag("td").get(3).text();
                    return type.compareTo(compareType);
                }
            };
            Collections.sort(trs, Collections.reverseOrder(comparator));
            for (Element tr : trs) {
                NotasFaltasDisciplinaEntity disciplinaEntity = new NotasFaltasDisciplinaEntity();
                String tdString = tr.getElementsByTag("td").get(0).text().trim();
                disciplinaEntity.setDisciplina(tdString);
                tdString = tr.getElementsByTag("td").get(1).text().trim();
                disciplinaEntity.setNomedisciplina(tdString);
                tdString = tr.getElementsByTag("td").get(2).text().trim();
                disciplinaEntity.setTurmaespecial(tdString);
                tdString = tr.getElementsByTag("td").get(3).text().trim();
                disciplinaEntity.setTipodisciplina(tdString);
                tdString = tr.getElementsByTag("td").get(4).text().trim();
                disciplinaEntity.setObsdisciplina(tdString);
                tdString = tr.getElementsByTag("td").get(5).text().trim();
                disciplinaEntity.setNp1(tdString);
                tdString = tr.getElementsByTag("td").get(6).text().trim();
                disciplinaEntity.setNp2(tdString);
                tdString = tr.getElementsByTag("td").get(7).text().trim();
                disciplinaEntity.setFaltas(tdString);
                disciplinaEntities.add(disciplinaEntity);
            }
            notasFaltasListEntity.setDisciplinas(disciplinaEntities);
            return notasFaltasListEntity;
        } catch (Exception e) {
            return null;
        }
    }
}
