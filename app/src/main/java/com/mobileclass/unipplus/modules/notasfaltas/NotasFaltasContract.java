package com.mobileclass.unipplus.modules.notasfaltas;

import com.mobileclass.unipplus.bases.BasePresenter;
import com.mobileclass.unipplus.bases.BaseView;
import com.mobileclass.unipplus.modules.notasfaltas.entities.NotasFaltasDisciplinaEntity;

import java.util.List;

public interface NotasFaltasContract {
    interface View extends BaseView {
        void setTableViewData(List<List<NotasFaltasDisciplinaEntity>> groupedDisciplinaEntities);
    }

    interface Presenter extends BasePresenter {

    }
}
