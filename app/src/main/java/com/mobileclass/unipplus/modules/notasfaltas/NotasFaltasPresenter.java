package com.mobileclass.unipplus.modules.notasfaltas;

import com.mobileclass.unipplus.modules.notasfaltas.entities.NotasFaltasEntity;
import com.mobileclass.unipplus.modules.notasfaltas.interactor.NotasFaltasInteractor;
import com.mobileclass.unipplus.modules.notasfaltas.interactor.NotasFaltasInteractorCallBack;

public class NotasFaltasPresenter implements NotasFaltasContract.Presenter, NotasFaltasInteractorCallBack {
    private NotasFaltasContract.View view;
    private NotasFaltasInteractor interactor;

    public NotasFaltasPresenter(NotasFaltasContract.View view) {
        this.view = view;
        interactor = new NotasFaltasInteractor();
        interactor.setCallBack(this);
    }

    @Override
    public void start() {
        view.showLoading(true);
        interactor.getNotasFaltas();
    }

    @Override
    public void onSuccess(NotasFaltasEntity notasFaltasEntity) {
        view.setTableViewData(notasFaltasEntity.getGroupedNotasFaltasList());
    }

    @Override
    public void onFailure(String errorMessage) {
        view.showAlertDialog("", errorMessage);
    }
}
