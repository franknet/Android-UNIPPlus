package com.mobileclass.unipplus.modules.home.view.activities;

public interface HomeView {
    void setSideMenuView();
    void setContentView();
    void showRateDialog(String message);
    void showExitDialog();
    boolean isSideMenuOpen();
    void openSideMenu();
    void closeSideMenu();
    void startGooglePlay();
    void finishHomeActivity();
    void startLoginActivity();
    void showInterstitialAd();
}
