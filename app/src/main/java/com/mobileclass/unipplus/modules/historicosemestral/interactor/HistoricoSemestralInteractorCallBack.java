package com.mobileclass.unipplus.modules.historicosemestral.interactor;

import com.mobileclass.unipplus.modules.historicosemestral.entities.HistoricoSemestralEntity;

public interface HistoricoSemestralInteractorCallBack {
    void onSuccess(HistoricoSemestralEntity historicoSemestralEntity);
    void onFailure(String error);
}
