package com.mobileclass.unipplus.modules.login;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.mobileclass.unipplus.R;
import com.mobileclass.unipplus.bases.BaseActivity;
import com.mobileclass.unipplus.customviews.FullScreenLoadingDialog;
import com.mobileclass.unipplus.modules.home.view.activities.HomeActivity;
import com.mobileclass.unipplus.modules.login.entity.LoginResponseEntity;
import com.mobileclass.unipplus.modules.login.entity.UserEntity;
import com.mobileclass.unipplus.services.google.admob.AdMobService;
import com.mobileclass.unipplus.services.google.admob.InterstitalAdListener;

import static android.view.View.OnClickListener;

public class LoginActivity extends BaseActivity implements TextWatcher, LoginContract.View {
    private EditText mEdtRA;
    private EditText mEdtSenha;
    private CheckBox mCbxCredentials;
    private FullScreenLoadingDialog mLoadingDialog;
    private LoginContract.Presenter mPresenter;

    //LIFE CIRCLE METHODS
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mEdtRA = findViewById(R.id.login_edt_ra);
        mEdtRA.addTextChangedListener(this);
        mEdtSenha = findViewById(R.id.login_edt_password);
        mEdtSenha.addTextChangedListener(this);
        mCbxCredentials = findViewById(R.id.login_ckb_credentials);
        mLoadingDialog = new FullScreenLoadingDialog(this);
        Button btnEnter = findViewById(R.id.login_btn_action);
        btnEnter.setOnClickListener(actionButtonClickListener());

        mPresenter = new LoginPresenter(this);
        mPresenter.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        sendScreenName("Login");
        View contentView = getWindow().getDecorView().getRootView();
        contentView.requestFocus();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mLoadingDialog.dismiss();
    }

    //VIEW METHODS
    @Override
    public void setRA(String ra) {
        mEdtRA.setText(ra);
    }

    @Override
    public void setPassword(String password) {
        mEdtSenha.setText(password);
    }

    @Override
    public void showAlertDialog(String title, String message) {
        AlertDialog.Builder alertExit = new AlertDialog.Builder(this);
        alertExit.setTitle(title);
        alertExit.setMessage(message);
        alertExit.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertExit.show();
    }

    @Override
    public void showNewVersionAlert() {
        AlertDialog.Builder alertExit = new AlertDialog.Builder(this);
        alertExit.setTitle(R.string.login_string_new_version_avaliable_title);
        alertExit.setMessage(R.string.login_string_new_version_avaliable_message);
        alertExit.setPositiveButton(R.string.login_string_new_version_avaliable_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startGooglePlay();
            }
        });
        alertExit.show();
    }

    @Override
    public void checkKeepCredentials(boolean checked) {
        mCbxCredentials.setChecked(checked);
    }

    @Override
    public void showLoading(boolean show) {
        if (show) {
            mLoadingDialog.show();
        } else {
            mLoadingDialog.dismiss();
        }
    }

    @Override
    public void hideKeyboard() {
        dismissKeyBoard();
    }

    @Override
    public void startHomeActivity(LoginResponseEntity result) {
        final Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra("result", result);
        startActivity(intent);
        showLoading(false);
    }

    //EVENTS
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) { }

    private OnClickListener actionButtonClickListener() {
        return new OnClickListener() {
            @Override
            public void onClick(View view) {
                UserEntity userEntity = new UserEntity();
                userEntity.setKeepCredentials(mCbxCredentials.isChecked());
                userEntity.setRa(mEdtRA.getText().toString());
                userEntity.setPassword(mEdtSenha.getText().toString());
                mPresenter.onEntrarButtonClicked(userEntity);
            }
        };
    }

    private void startGooglePlay() {
        Uri uri = Uri.parse("market://details?id=com.mobileclass.unipplus");
        Intent googlePlay = new Intent(Intent.ACTION_VIEW, uri);
        googlePlay.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(googlePlay);
    }
}
