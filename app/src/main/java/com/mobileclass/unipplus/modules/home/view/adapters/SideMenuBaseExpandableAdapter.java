package com.mobileclass.unipplus.modules.home.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobileclass.unipplus.modules.home.entities.SideMenuListEntity;
import com.mobileclass.unipplus.R;

import java.util.ArrayList;

public class SideMenuBaseExpandableAdapter extends BaseExpandableListAdapter {
    private Context context;
    private ArrayList<SideMenuListEntity> sideMenuListEntities;
    private LayoutInflater layoutInflater;

    public SideMenuBaseExpandableAdapter(Context context, ArrayList<SideMenuListEntity> sideMenuListEntities) {
        this.context = context;
        this.sideMenuListEntities = sideMenuListEntities;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getGroupCount() {
        return this.sideMenuListEntities.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int childrenCount;

        if (groupPosition == 0) {
            childrenCount = sideMenuListEntities.get(0).getChildren().size();
        } else {
            childrenCount = 0;
        }

        return childrenCount;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return sideMenuListEntities.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        Object child;

        if (groupPosition == 0) {
            child = sideMenuListEntities.get(groupPosition).getChildren().get(childPosition);
        } else {
            child = null;
        }

        return child;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return sideMenuListEntities.get(groupPosition).getId();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = this.layoutInflater.inflate(R.layout.list_group_item_side_menu, parent, false);

            viewHolder.imvIcon = (ImageView) convertView.findViewById(R.id.imvGroupIcon);
            viewHolder.txvTitle = (TextView) convertView.findViewById(R.id.txvGroupTitle);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.imvIcon.setImageDrawable(this.context.getResources().getDrawable(this.sideMenuListEntities.get(groupPosition).getIcon()));
        viewHolder.txvTitle.setText(this.sideMenuListEntities.get(groupPosition).getTitle());

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = this.layoutInflater.inflate(R.layout.list_child_item_side_menu, parent, false);

            viewHolder.imvIcon = (ImageView) convertView.findViewById(R.id.imvGroupIcon);
            viewHolder.txvTitle = (TextView) convertView.findViewById(R.id.txvGroupTitle);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (groupPosition == 0) {
            viewHolder.imvIcon.setImageDrawable(this.sideMenuListEntities.get(groupPosition).getChildren().get(childPosition).getIcon());
            viewHolder.txvTitle.setText(this.sideMenuListEntities.get(groupPosition).getChildren().get(childPosition).getTitle());
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    private class ViewHolder {
        private ImageView imvIcon;
        private TextView txvTitle;
    }
}
