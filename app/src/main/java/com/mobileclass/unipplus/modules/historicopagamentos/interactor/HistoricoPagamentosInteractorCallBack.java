package com.mobileclass.unipplus.modules.historicopagamentos.interactor;

import com.mobileclass.unipplus.modules.historicopagamentos.entities.HistoricoPagamentosEntity;

public interface HistoricoPagamentosInteractorCallBack {
    void onSuccess(HistoricoPagamentosEntity historicoPagamentosEntity);
    void onFailure(String errorMessage);
    void totalBills(String totalBills);
}
