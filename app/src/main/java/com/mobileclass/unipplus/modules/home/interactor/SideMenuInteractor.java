package com.mobileclass.unipplus.modules.home.interactor;

import com.mobileclass.unipplus.R;
import com.mobileclass.unipplus.modules.home.entities.SideMenuListEntity;

import java.util.ArrayList;

public class SideMenuInteractor {
    private SideMenuInteractorCallBack callBack;

    public void setCallBack(SideMenuInteractorCallBack callBack) {
        this.callBack = callBack;
    }

    public void loadSideMenuContents() {
        ArrayList<SideMenuListEntity> sideMenuListEntities = new ArrayList<>();
        SideMenuListEntity sideMenuListEntity;

//        sideMenuListEntity = new SideMenuListEntity();
//        sideMenuListEntity.setIcon(R.drawable.ic_action_library);
//        sideMenuListEntity.setTitle("BIBLIOTECA");
//        sideMenuListEntity.setId(SideMenuListEntity.BIBLIOTECA_ID);
//        sideMenuListEntity.setChildren(new ArrayList<SideMenuListEntity.Child>());
//
//        sideMenuListEntities.add(sideMenuListEntity);

//        sideMenuListEntity = new SideMenuListEntity();
//        sideMenuListEntity.setIcon(R.drawable.ic_action_white_time);
//        sideMenuListEntity.setTitle("HORÁRIOS DE AULAS");
//        sideMenuListEntity.setId(SideMenuListEntity.HORARIO_AULAS_ID);
//        sideMenuListEntity.setChildren(new ArrayList<SideMenuListEntity.Child>());
//
//        sideMenuListEntities.add(sideMenuListEntity);

        sideMenuListEntity = new SideMenuListEntity();
        sideMenuListEntity.setIcon(R.drawable.ic_action_share);
        sideMenuListEntity.setTitle("COMPARTILHAR");
        sideMenuListEntity.setId(SideMenuListEntity.COMPARTILHAR_ID);
        sideMenuListEntity.setChildren(new ArrayList<SideMenuListEntity.Child>());

        sideMenuListEntities.add(sideMenuListEntity);

        sideMenuListEntity = new SideMenuListEntity();
        sideMenuListEntity.setIcon(R.drawable.ic_action_rate);
        sideMenuListEntity.setTitle("AVALIAR");
        sideMenuListEntity.setId(SideMenuListEntity.AVALIAR_ID);
        sideMenuListEntity.setChildren(new ArrayList<SideMenuListEntity.Child>());

        sideMenuListEntities.add(sideMenuListEntity);

        this.callBack.onContentsLoaded(sideMenuListEntities);
    }
}
