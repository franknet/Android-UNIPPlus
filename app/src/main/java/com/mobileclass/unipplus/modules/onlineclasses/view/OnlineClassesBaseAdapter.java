package com.mobileclass.unipplus.modules.onlineclasses.view;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdView;
import com.mobileclass.unipplus.R;
import com.mobileclass.unipplus.modules.onlineclasses.entities.OnlineClassEntity;
import com.mobileclass.unipplus.services.google.admob.AdMobService;
import com.mobileclass.unipplus.services.google.admob.BannerAdListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OnlineClassesBaseAdapter extends RecyclerView.Adapter<OnlineClassesBaseAdapter.ViewHolder> {
    private final Context context;
    private final List<OnlineClassEntity> classes;
    private final Map<OnlineClassEntity.StatusCode, Integer> statusColor = new HashMap<OnlineClassEntity.StatusCode, Integer>() { {
        put(OnlineClassEntity.StatusCode.ON_LIVE, android.R.color.holo_blue_light);
        put(OnlineClassEntity.StatusCode.HAS_PASSED, android.R.color.holo_red_light);
        put(OnlineClassEntity.StatusCode.PENDING, android.R.color.black);
    } };
    private OnlineClassesBaseAdapterOnItemSelected listener;

    public OnlineClassesBaseAdapter(Context context, List<OnlineClassEntity> classes) {
        this.context = context;
        this.classes = classes;
    }

    public void setListener(OnlineClassesBaseAdapterOnItemSelected listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.list_child_item_online_classes, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final OnlineClassEntity onlineClass = this.classes.get(position);
        holder.setDiscipline(onlineClass.getDiscipline());
        holder.setDatetime(onlineClass.getDatetime());
        holder.setStatus(onlineClass.getStatus());
        holder.setStatusCode(onlineClass.getStatusCode());
        holder.setLink(onlineClass.getLink());
        holder.setModel(onlineClass);
        holder.itemView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    listener.onSelect(onlineClass);
                }
                return false;
            }
        });
        if (position == 0) {
            holder.loadAdBanner();
        }
    }

    @Override
    public int getItemCount() {
        if (this.classes != null) {
            return this.classes.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView txvDiscipline;
        private final TextView txvDatetime;
        private final TextView txvStatus;
        private final AdView advBanner;
        private String link;
        private OnlineClassEntity model;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txvDiscipline = itemView.findViewById(R.id.txvOnlineClassDiscipline);
            txvDatetime = itemView.findViewById(R.id.txvOnlineClassDatetime);
            txvStatus = itemView.findViewById(R.id.txvOnlineClassStatus);
            advBanner = itemView.findViewById(R.id.advBanner);
            advBanner.setVisibility(View.GONE);
        }

        public void setModel(OnlineClassEntity model) {
            this.model = model;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public void setDiscipline(String discipline) {
            this.txvDiscipline.setText(discipline);
        }

        public void setDatetime(String datetime) {
            this.txvDatetime.setText(datetime);
        }

        public void setStatus(String status) {
            this.txvStatus.setText(status);
        }

        public void setStatusCode(OnlineClassEntity.StatusCode code) {
            this.txvStatus.setTextColor(itemView.getResources().getColor(statusColor.get(code)));
        }

        public void loadAdBanner() {
            AdMobService.requestBannerAd(this.advBanner, new BannerAdListener() {
                @Override
                public void onAdLoadFinished(AdView adView) {
                    adView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAdLoadFinishedWithError(AdView adView, String message) {
                }
            });
        }
    }
}
