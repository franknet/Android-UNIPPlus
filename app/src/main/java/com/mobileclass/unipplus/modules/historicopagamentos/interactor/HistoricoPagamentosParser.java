package com.mobileclass.unipplus.modules.historicopagamentos.interactor;

import com.mobileclass.unipplus.modules.historicopagamentos.entities.HistoricoPagamentosListEntity;
import com.mobileclass.unipplus.modules.historicopagamentos.entities.HistoricoPagamentosPosicaoEntity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class HistoricoPagamentosParser {
    static HistoricoPagamentosListEntity parse(byte[] data) {
        try {
            HistoricoPagamentosListEntity historicoPagamentosListEntity = new HistoricoPagamentosListEntity();
            ArrayList<HistoricoPagamentosPosicaoEntity> posicao = new ArrayList<>();
            String html = new String(data);
            Document document = Jsoup.parse(html);
            Element table = document.getElementsByClass("table-striped").first();
            Elements trs = table.getElementsByTag("tr");
            trs.remove(0);
            Comparator<Element> comparator = new Comparator<Element>() {
                @Override
                public int compare(Element lhs, Element rhs) {
                    String type = lhs.getElementsByTag("td").get(2).text();
                    String compareType = rhs.getElementsByTag("td").get(2).text();
                    return type.compareTo(compareType);
                }
            };
            Collections.sort(trs, Collections.reverseOrder(comparator));
            for (Element tr : trs) {
                HistoricoPagamentosPosicaoEntity entity = new HistoricoPagamentosPosicaoEntity();
                String tdValue = tr.getElementsByTag("td").get(0).text();
                entity.setSequencia(tdValue);
                tdValue = tr.getElementsByTag("td").get(1).text();
                entity.setParcela(tdValue);
                tdValue = tr.getElementsByTag("td").get(2).text();
                entity.setTipoparcela(tdValue);
                entity.setDescricaodivida(getGroupTitleByBillType(tdValue.trim()));
                tdValue = tr.getElementsByTag("td").get(4).text();
                entity.setDatavencimento(tdValue);
                tdValue = tr.getElementsByTag("td").get(5).text();
                tdValue = tdValue.replace("R$", "R$ ");
                entity.setValor(tdValue);
                tdValue = tr.getElementsByTag("td").get(6).text();
                entity.setDatapagamento(tdValue);
                tdValue = tr.getElementsByTag("td").get(7).text();
                tdValue = tdValue.replace("R$", "");
                entity.setValorpago(tdValue);
                tdValue = tr.getElementsByTag("td").get(8).text();
                entity.setEstorno(tdValue);
                tdValue = tr.getElementsByTag("td").get(9).text();
                entity.setSituacao(tdValue);
                posicao.add(entity);
            }
            historicoPagamentosListEntity.setPosicao(posicao);
            return historicoPagamentosListEntity;
        } catch (Exception e) {
            return null;
        }
    }

    private static String getGroupTitleByBillType(String type) {
        switch (type) {
            case "MS":
                return "Mensalidade";
            case "DF":
                return "Diferença de mensalidade";
            case "CD":
                return "Confissão de dívida";
            case "PC":
                return "Parcelamento de mensalidades";
            case "TX":
                return "Taxa de serviços";
            default:
                return "";
        }
    }
}

