package com.mobileclass.unipplus.modules.home.interactor;

import com.mobileclass.unipplus.UNIPPlusApplication;
import com.mobileclass.unipplus.modules.home.entities.RateEntity;

public class HomeInteractor {
    private HomeInteractorCallBack callBack;
    private RateEntity entity;

    public void setCallBack(HomeInteractorCallBack callBack) {
        this.callBack = callBack;
    }

    public void checkRate() {
        this.entity = UNIPPlusApplication.getInstance().getCache().getRateEntity();
        validateRateEntity();
    }

    public void stopValidation() {
        this.entity.setCount(0);
        this.entity.setValidateCount(false);
        UNIPPlusApplication.getInstance().getCache().setRateEntity(this.entity);
    }

    //PRIVATE METHODS
    private void validateRateEntity() {
        if (this.entity != null) {
            mustRateValidation();
        }
    }

    private void mustRateValidation() {
        int versionCode = (int) UNIPPlusApplication.getInstance().getInfo().getAppVersionCode();
        if (this.entity.getVersionCode() == 0) {
            this.entity.setValidateCount(true);
            this.entity.setVersionCode(versionCode);
        }
        if (this.entity.isValidateCount()) {
            if (this.entity.getCount() == 3) {
                this.entity.setValidateCount(false);
                this.callBack.mustRate("Sua opinião é importante, deixe um comentário na Google Play e mostre para os outros alunos sua satisfação com o aplicativo UNIP Plus");
            } else {
                this.entity.setCount(this.entity.getCount() + 1);
            }
            UNIPPlusApplication.getInstance().getCache().setRateEntity(this.entity);
        }
    }
}
