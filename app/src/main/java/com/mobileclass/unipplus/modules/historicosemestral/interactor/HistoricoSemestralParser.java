package com.mobileclass.unipplus.modules.historicosemestral.interactor;

import com.mobileclass.unipplus.modules.historicosemestral.entities.HistoricoSemestralDisciplinaEntity;
import com.mobileclass.unipplus.modules.historicosemestral.entities.HistoricoSemestralSemestreEntity;
import com.mobileclass.unipplus.modules.historicosemestral.entities.HistoricoSemestralEntity;
import com.mobileclass.unipplus.utils.TextUtils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class HistoricoSemestralParser {
    static HistoricoSemestralEntity parse(byte[] data) {
        try {
            HistoricoSemestralEntity historicoSemestralEntity = new HistoricoSemestralEntity();
            ArrayList<HistoricoSemestralSemestreEntity> semestres = new ArrayList<>();
            String html = new String(data);
            Document document = Jsoup.parse(html);
            Element table = document.getElementsByClass("table-striped").first();
            Elements trs = table.getElementsByTag("tr");
            trs.remove(0);
            Comparator<Element> comparator = new Comparator<Element>() {

                @Override
                public int compare(Element lhs, Element rhs) {
                    String semester = lhs.getElementsByTag("td").get(0).text().trim().replace(".", "");
                    String compareSemester = rhs.getElementsByTag("td").get(0).text().trim().replace(".", "");
                    return semester.equalsIgnoreCase(compareSemester) ? 0 : 1;
                }
            };
            Collections.sort(trs, comparator);

            for (Element tr : trs) {
                if (!tr.getElementsByTag("td").get(0).text().isEmpty()) {
                    HistoricoSemestralDisciplinaEntity disciplinaEntity = new HistoricoSemestralDisciplinaEntity();
                    String tdValue = tr.getElementsByTag("td").get(0).text();
                    disciplinaEntity.setSemestre(tdValue + " º Semestre");
                    tdValue = tr.getElementsByTag("td").get(1).text();
                    disciplinaEntity.setCodigo(tdValue);
                    tdValue = tr.getElementsByTag("td").get(2).text();
                    disciplinaEntity.setNomedisciplina(tdValue);
                    tdValue = tr.getElementsByTag("td").get(3).text();
                    disciplinaEntity.setCargahoraria(tdValue);
                    tdValue = tr.getElementsByTag("td").get(4).text();
                    disciplinaEntity.setMedia(tdValue);
                    tdValue = tr.getElementsByTag("td").get(5).text();
                    disciplinaEntity.setAno(tdValue);
                    tdValue = tr.getElementsByTag("td").get(6).text();
                    disciplinaEntity.setSituacao(tdValue);
                    groupSemesters(semestres, disciplinaEntity);
                }
            }
            historicoSemestralEntity.setSemestres(semestres);
            return historicoSemestralEntity;
        } catch (Exception e) {
            return null;
        }
    }

    private static void groupSemesters(ArrayList<HistoricoSemestralSemestreEntity> semestres, HistoricoSemestralDisciplinaEntity disciplinaEntity) {
        if (semestres.isEmpty()) {
            semestres.add(addSemester(disciplinaEntity));
        } else {
            addDiscipline(semestres, disciplinaEntity);
        }
    }

    private static void addDiscipline(ArrayList<HistoricoSemestralSemestreEntity> semestres, HistoricoSemestralDisciplinaEntity disciplinaEntity) {
        HistoricoSemestralSemestreEntity semestreEntity = semestres.get(semestres.size()-1);
        if (disciplinaEntity.getSemestre().equalsIgnoreCase(semestreEntity.getSemestre())) {
            semestreEntity.getDisciplinas().add(disciplinaEntity);
        } else {
            semestres.add(addSemester(disciplinaEntity));
        }
    }

    private static HistoricoSemestralSemestreEntity addSemester(HistoricoSemestralDisciplinaEntity disciplinaEntity) {
        HistoricoSemestralSemestreEntity semestreEntity = new HistoricoSemestralSemestreEntity();
        ArrayList<HistoricoSemestralDisciplinaEntity> disciplinas = new ArrayList<>();
        disciplinas.add(disciplinaEntity);
        semestreEntity.setDisciplina(disciplinas);
        semestreEntity.setSemestre(disciplinaEntity.getSemestre());
        return semestreEntity;
    }
}
