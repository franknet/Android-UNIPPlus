package com.mobileclass.unipplus.modules.home.view.fragments;

import android.content.Intent;
import android.graphics.Bitmap;

import com.mobileclass.unipplus.modules.home.entities.SideMenuListEntity;

import java.util.ArrayList;

/**
 * Created by hackintosh on 19/03/2017.
 */

public interface SideMenuView {
    void setMenuContents(ArrayList<SideMenuListEntity> sideMenuListEntities);
    void setUserName(String name);
    void setCourseName(String course);
    void setUserProfile(Bitmap profile);
    void goTo(Intent intent);
    void startLibrayActivity();
}
