package com.mobileclass.unipplus.modules.home.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.mobileclass.unipplus.R;
import com.mobileclass.unipplus.modules.home.presenter.ContentPresenter;
import com.mobileclass.unipplus.modules.home.view.adapters.ViewPagerAdapter;
import com.mobileclass.unipplus.bases.BaseActivity;
import com.mobileclass.unipplus.bases.BaseFragment;
import com.mobileclass.unipplus.customviews.CustomViewPager;

import java.util.List;

public class ContentFragment extends BaseFragment implements ContentView, CustomViewPager.CustomViewPagerListener {
    private CustomViewPager cvpContent;
    private BaseActivity activity;
    private ContentPresenter presenter;
    private Toolbar toolbar;

    public ContentFragment(BaseActivity activity) {
        this.activity = activity;
    }

    //FRAGMENT LIFE CIRCLE
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_content, container, false);
        this.toolbar =  view.findViewById(R.id.toolbar);
        this.cvpContent = view.findViewById(R.id.cvp_content);
        this.cvpContent.setListener(this);

        this.presenter = new ContentPresenter(this);

        return view;
    }

    //EVENTS
    @Override
    public void onPageSelected(int position) {
        if (this.presenter != null) {
            this.presenter.contentSelected(position);
        }
    }

    //PRESENTER METHODS
    @Override
    public void setToolbar() {
        if (this.toolbar != null) {
            this.toolbar.setNavigationIcon(R.drawable.ic_action_nav);
            this.toolbar.setTitle("Aulas Online");
            this.activity.setSupportActionBar(toolbar);
        }
    }

    @Override
    public void setContentsList(List<Fragment> contents) {
        this.cvpContent.setAdapter(new ViewPagerAdapter(getChildFragmentManager(), contents));
        this.cvpContent.setCurrentItem(0, false);
    }

    @Override
    public void setCurrentPageTitle(String title) {
        if (this.toolbar != null) {
            this.toolbar.setTitle(title);
            sendScreenName(title);
        }
    }

}
