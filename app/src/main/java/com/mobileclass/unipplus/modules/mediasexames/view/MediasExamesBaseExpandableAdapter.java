package com.mobileclass.unipplus.modules.mediasexames.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mobileclass.unipplus.R;
import com.mobileclass.unipplus.customviews.CircularProgressbar;
import com.mobileclass.unipplus.modules.mediasexames.entities.MediasExamesDisciplinaEntity;
import com.mobileclass.unipplus.services.google.admob.AdMobService;
import com.mobileclass.unipplus.services.google.admob.BannerAdListener;
import com.mobileclass.unipplus.utils.NumberUtils;

import java.util.List;

public class MediasExamesBaseExpandableAdapter extends BaseExpandableListAdapter {
    private LayoutInflater layoutInflater;
    private List<List<MediasExamesDisciplinaEntity>> groupedMediasExamesDisciplinas;
    
    public MediasExamesBaseExpandableAdapter(List<List<MediasExamesDisciplinaEntity>> groupedMediasExamesList, Context context) {
        this.groupedMediasExamesDisciplinas = groupedMediasExamesList;
        this.layoutInflater = LayoutInflater.from(context);
    }
    
    @Override
    public int getGroupCount() {
        return groupedMediasExamesDisciplinas.size();
    }

    @Override
    public int getChildrenCount(int i) {
        List<MediasExamesDisciplinaEntity> mediasExamesDisciplinaEntities = groupedMediasExamesDisciplinas.get(i);
        return mediasExamesDisciplinaEntities.size();
    }

    @Override
    public Object getGroup(int i) {
        return null;
    }

    @Override
    public Object getChild(int i, int i1) {
        return null;
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        List<MediasExamesDisciplinaEntity> mediasExamesDisciplinaEntities = groupedMediasExamesDisciplinas.get(i);
        MediasExamesDisciplinaEntity disciplinaEntity = mediasExamesDisciplinaEntities.get(0);
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = this.layoutInflater.inflate(R.layout.list_group_item_default, parent, false);
            viewHolder.txvGroupTitle = (TextView) convertView.findViewById(R.id.txvGroupTitle);
            viewHolder.txvGroupCount = (TextView) convertView.findViewById(R.id.txvGroupCount);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.txvGroupTitle.setText(disciplinaEntity.getTipodisciplina());
        viewHolder.txvGroupCount.setText(String.valueOf(mediasExamesDisciplinaEntities.size()));
        return convertView;
    }

    @Override
    public View getChildView(int groupIndex, int childIndex, boolean b, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        List<MediasExamesDisciplinaEntity> mediasExamesDisciplinaEntities = groupedMediasExamesDisciplinas.get(groupIndex);
        MediasExamesDisciplinaEntity disciplinaEntity = mediasExamesDisciplinaEntities.get(childIndex);
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = this.layoutInflater.inflate(R.layout.list_child_item_medias_exames, parent, false);
            viewHolder.txvMediasExamesDisciplina    = (TextView) convertView.findViewById(R.id.txvMediasExamesDisciplina);
            viewHolder.txvMediasExamesMS            = (TextView) convertView.findViewById(R.id.txvMediasExamesMS);
            viewHolder.txvMediasExamesEX            = (TextView) convertView.findViewById(R.id.txvMediasExamesEX);
            viewHolder.txvMediasExamesMF            = (TextView) convertView.findViewById(R.id.txvMediasExamesMF);
            viewHolder.cpbMediasExamesMS            = (CircularProgressbar) convertView.findViewById(R.id.cpbMediasExamesMS);
            viewHolder.cpbMediasExamesEX            = (CircularProgressbar) convertView.findViewById(R.id.cpbMediasExamesEX);
            viewHolder.cpbMediasExamesMF            = (CircularProgressbar) convertView.findViewById(R.id.cpbMediasExamesMF);
            viewHolder.txvExAdvise                  = (TextView) convertView.findViewById(R.id.txvExAdvise);
            viewHolder.imvStatus                    = (ImageView) convertView.findViewById(R.id.imvStatus);
            viewHolder.advBanner                    = (AdView) convertView.findViewById(R.id.advBanner);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        int bannerIndex = groupedMediasExamesDisciplinas.get(groupIndex).size() / 2;
        if (childIndex == bannerIndex) {
            AdMobService.requestBannerAd(viewHolder.advBanner, new BannerAdListener() {
                @Override
                public void onAdLoadFinished(AdView adView) {
                    adView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAdLoadFinishedWithError(AdView adView, String message) {
                }
            });
        } else {
            viewHolder.advBanner.setVisibility(View.GONE);
        }

        Double ms = NumberUtils.decimalStringToDouble(disciplinaEntity.getMediasemestral());
        Double ex = NumberUtils.decimalStringToDouble(disciplinaEntity.getExame());
        Double mf = NumberUtils.decimalStringToDouble(disciplinaEntity.getMediafinal());
        viewHolder.txvMediasExamesDisciplina.setText(disciplinaEntity.getNomedisciplina());
        viewHolder.txvMediasExamesMS.setText(String.valueOf(ms));
        viewHolder.txvMediasExamesEX.setText(String.valueOf(ex));
        viewHolder.txvMediasExamesMF.setText(String.valueOf(mf));
        viewHolder.txvExAdvise.setText(disciplinaEntity.getStatus());
        viewHolder.imvStatus.setImageResource(disciplinaEntity.getStatusImage());
        int mediaSemestralColor = convertView.getContext().getResources().getColor(disciplinaEntity.getMediaSemestralColor());
        viewHolder.txvMediasExamesMS.setTextColor(mediaSemestralColor);
        viewHolder.cpbMediasExamesMS.setColor(mediaSemestralColor);
        viewHolder.cpbMediasExamesMS.setProgress(NumberUtils.decimalStringToInt(disciplinaEntity.getMediasemestral()));

        int exameColor = convertView.getContext().getResources().getColor(disciplinaEntity.getExameColor());
        viewHolder.txvMediasExamesEX.setTextColor(exameColor);
        viewHolder.cpbMediasExamesEX.setColor(exameColor);
        viewHolder.cpbMediasExamesEX.setProgress(NumberUtils.decimalStringToInt(disciplinaEntity.getExame()));

        int mediaFinallColor = convertView.getContext().getResources().getColor(disciplinaEntity.getMediaFinalColor());
        viewHolder.txvMediasExamesMF.setTextColor(mediaFinallColor);
        viewHolder.cpbMediasExamesMF.setColor(mediaFinallColor);
        viewHolder.cpbMediasExamesMF.setProgress(NumberUtils.decimalStringToInt(disciplinaEntity.getMediafinal()));

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }

    private class ViewHolder {
        AdView advBanner;
        TextView txvMediasExamesDisciplina;
        TextView txvMediasExamesMS;
        TextView txvMediasExamesEX;
        TextView txvMediasExamesMF;
        TextView txvGroupTitle;
        TextView txvGroupCount;
        TextView txvExAdvise;
        ImageView imvStatus;
        CircularProgressbar cpbMediasExamesMS;
        CircularProgressbar cpbMediasExamesEX;
        CircularProgressbar cpbMediasExamesMF;
    }
}
