package com.mobileclass.unipplus.modules.historicopagamentos.entities;

public class HistoricoPagamentosPosicaoEntity {
    private String anoparcela;
    private String coddivida;
    private String datapagamento;
    private String datavencimento;
    private String descricaodivida;
    private String estorno;
    private String numacordo;
    private String numparcela;
    private String parcela;
    private String sequencia;
    private String situacao;
    private int situacaoColor;
    private String tipoparcela;
    private String valor;
    private String valorpago;

    public String getAnoparcela() {
        return anoparcela;
    }

    public void setAnoparcela(String anoparcela) {
        this.anoparcela = anoparcela;
    }

    public String getCoddivida() {
        return coddivida;
    }

    public void setCoddivida(String coddivida) {
        this.coddivida = coddivida;
    }

    public String getDatapagamento() {
        return datapagamento;
    }

    public void setDatapagamento(String datapagamento) {
        this.datapagamento = datapagamento;
    }

    public String getDatavencimento() {
        return datavencimento;
    }

    public void setDatavencimento(String datavencimento) {
        this.datavencimento = datavencimento;
    }

    public String getDescricaodivida() {
        return descricaodivida;
    }

    public void setDescricaodivida(String descricaodivida) {
        this.descricaodivida = descricaodivida;
    }

    public String getEstorno() {
        return estorno;
    }

    public void setEstorno(String estorno) {
        this.estorno = estorno;
    }

    public String getNumacordo() {
        return numacordo;
    }

    public void setNumacordo(String numacordo) {
        this.numacordo = numacordo;
    }

    public String getNumparcela() {
        return numparcela;
    }

    public void setNumparcela(String numparcela) {
        this.numparcela = numparcela;
    }

    public String getParcela() {
        return parcela;
    }

    public void setParcela(String parcela) {
        this.parcela = parcela;
    }

    public String getSequencia() {
        return sequencia;
    }

    public void setSequencia(String sequencia) {
        this.sequencia = sequencia;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public int getSituacaoColor() {
        return situacaoColor;
    }

    public void setSituacaoColor(int situacaoColor) {
        this.situacaoColor = situacaoColor;
    }

    public String getTipoparcela() {
        return tipoparcela;
    }

    public void setTipoparcela(String tipoparcela) {
        this.tipoparcela = tipoparcela;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getValorpago() {
        return valorpago;
    }

    public void setValorpago(String valorpago) {
        this.valorpago = valorpago;
    }
}
