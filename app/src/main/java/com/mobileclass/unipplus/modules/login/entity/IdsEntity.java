package com.mobileclass.unipplus.modules.login.entity;

public class IdsEntity {
    private String ra_id;
    private String image_id;
    private String password_id;
    private String captcha_id;
    private String error_msg_id;

    public IdsEntity(String csv) {
        String[] ids = csv.split("\\|");
        this.ra_id = ids[0];
        this.image_id = ids[1];
        this.password_id = ids[2];
        this.captcha_id = ids[3];
        this.error_msg_id = ids[4];
    }

    public String getRa_id() {
        return ra_id;
    }

    public String getImage_id() {
        return image_id;
    }

    public String getPassword_id() {
        return password_id;
    }

    public String getCaptcha_id() {
        return captcha_id;
    }

    public String getError_msg_id() {
        return error_msg_id;
    }
}
