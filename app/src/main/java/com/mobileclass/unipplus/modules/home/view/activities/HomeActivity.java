package com.mobileclass.unipplus.modules.home.view.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.Window;

import androidx.annotation.NonNull;

import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.mobileclass.unipplus.R;
import com.mobileclass.unipplus.bases.BaseActivity;
import com.mobileclass.unipplus.customviews.ResizeMenuLayout;
import com.mobileclass.unipplus.modules.home.presenter.HomePresenter;
import com.mobileclass.unipplus.modules.home.view.fragments.ContentFragment;
import com.mobileclass.unipplus.modules.home.view.fragments.SideMenuFragment;
import com.mobileclass.unipplus.modules.login.LoginActivity;
import com.mobileclass.unipplus.services.google.admob.AdMobService;
import com.mobileclass.unipplus.services.google.admob.InterstitalAdListener;

public class HomeActivity extends BaseActivity implements HomeView {
    private HomePresenter presenter;
    private ResizeMenuLayout resizeMenuLayout;

    //LIFE CIRCLE METHODS
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.resizeMenuLayout = new ResizeMenuLayout(this, getSupportFragmentManager());
        setContentView(this.resizeMenuLayout);
        this.presenter = new HomePresenter(this);
        showInterstitialAd();
    }

    @Override
    protected void onResume() {
        super.onResume();
        sendScreenName("Notas e Faltas");
    }

    @Override
    protected void onStop() {
        super.onStop();
        closeSideMenu();
    }

    //VIEW METHODS
    @Override
    public void setSideMenuView() {
        this.resizeMenuLayout.setMenuLeft(new SideMenuFragment());
    }

    @Override
    public void setContentView() {
        this.resizeMenuLayout.setContentMenu(new ContentFragment(this));
    }

    @Override
    public void showRateDialog(String message) {
        AlertDialog.Builder rateDialog = new AlertDialog.Builder(this);
        rateDialog.setTitle("Avalie o UNIP Plus!");
        rateDialog.setMessage(message);
        rateDialog.setPositiveButton("Avaliar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startGooglePlay();
                dialog.dismiss();
            }
        });
        rateDialog.setNegativeButton("Já avaliei", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                presenter.onJaAvalieiClicked();
                dialog.dismiss();
            }
        });
        rateDialog.show();
    }

    @Override
    public void showExitDialog() {
        AlertDialog.Builder alertExit = new AlertDialog.Builder(this);
        alertExit.setTitle("");
        alertExit.setMessage(getString(R.string.main_exit_message));
        alertExit.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finishHomeActivity();
            }
        });
        alertExit.setNegativeButton("Não", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertExit.show();
    }

    @Override
    public boolean isSideMenuOpen() {
        return this.resizeMenuLayout.isOpen();
    }

    @Override
    public void openSideMenu() {
        this.resizeMenuLayout.openLeftMenu();
    }

    @Override
    public void closeSideMenu() {
        this.resizeMenuLayout.closeLeftMenu();
    }

    @Override
    public void startGooglePlay() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent googlePlay = new Intent(Intent.ACTION_VIEW, uri);
        googlePlay.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(googlePlay);
    }

    @Override
    public void finishHomeActivity() {
        finish();
    }

    @Override
    public void startLoginActivity() {
        Intent intentLogin = new Intent(this, LoginActivity.class);
        startActivity(intentLogin);
    }

    @Override
    public void showInterstitialAd() {
        final Activity activity = this;

        AdMobService.requestInterstitialAd(this, new InterstitalAdListener() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                interstitialAd.show(activity);
            }

            @Override
            public void onAdClosed() {  }

            @Override
            public void onAdFailedToLoad(String error) {  }
        });
    }

    //EVENTS
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.presenter.onHomeClicked();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        this.presenter.onBackButtonClicked();
    }
}
