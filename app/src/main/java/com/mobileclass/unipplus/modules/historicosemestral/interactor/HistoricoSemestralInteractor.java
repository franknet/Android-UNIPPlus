package com.mobileclass.unipplus.modules.historicosemestral.interactor;

import com.mobileclass.unipplus.R;
import com.mobileclass.unipplus.modules.historicosemestral.HistoricoSemestralProvider;
import com.mobileclass.unipplus.modules.historicosemestral.entities.HistoricoSemestralSemestreEntity;
import com.mobileclass.unipplus.modules.historicosemestral.entities.HistoricoSemestralDisciplinaEntity;
import com.mobileclass.unipplus.modules.historicosemestral.entities.HistoricoSemestralEntity;
import com.mobileclass.unipplus.net.LinksUNIP;
import com.mobileclass.unipplus.net.http.HttpClient;
import com.mobileclass.unipplus.utils.NumberUtils;

public class HistoricoSemestralInteractor {
    private static final String INTERNAL_ERROR = "Ocorreu um erro iniesperado, por favor tente novamente.";
    private HistoricoSemestralInteractorCallBack callBack;

    public void setCallBack(HistoricoSemestralInteractorCallBack callBack) {
        this.callBack = callBack;
    }

    public void getHistoricoSemestral() {
        HistoricoSemestralProvider provider = new HistoricoSemestralProvider();

        HttpClient client = new HttpClient();
        client.doRequest(provider);
        client.result(new HttpClient.Result() {
            @Override
            public void onSuccess(int statusCode, byte[] data) {
                if (statusCode == 200) {
                    createHistoricoSemestralEntity(data);
                } else {
                    callBack.onFailure("Sessão expirada, logue-se novamente");
                }
            }

            @Override
            public void onFailure(String message) {
                callBack.onFailure(message);
            }
        });
        client.execute();
    }

    //PRIVATE METHODS
    private void createHistoricoSemestralEntity(byte[] data) {
        HistoricoSemestralEntity historicoSemestralEntity = HistoricoSemestralParser.parse(data);
        validateHistoricoSemestralEntity(historicoSemestralEntity);
    }

    private void validateHistoricoSemestralEntity(HistoricoSemestralEntity historicoSemestralEntity) {
        if (historicoSemestralEntity != null) {
            if (historicoSemestralEntity.getSemestres().isEmpty()) {
                callBack.onFailure("Relação de disciplinas não encontrada.");
            } else {
                for (HistoricoSemestralSemestreEntity semestreEntity : historicoSemestralEntity.getSemestres()) {
                    validateAproveitamento(semestreEntity);
                }
                callBack.onSuccess(historicoSemestralEntity);
            }
        } else {
            callBack.onFailure(INTERNAL_ERROR);
        }
    }

    private void validateAproveitamento(HistoricoSemestralSemestreEntity historicoSemestralSemestreEntity) {
        int aproveitamento = 0;
        boolean semetreCursado = false;

        for (HistoricoSemestralDisciplinaEntity disciplinaEntity : historicoSemestralSemestreEntity.getDisciplinas()) {
            if (disciplinaEntity.getMedia().trim().equalsIgnoreCase("--") && disciplinaEntity.getSituacao().trim().equalsIgnoreCase("aprovado")) {
                disciplinaEntity.setMedia("10,0");
            }

            if (disciplinaEntity.getMedia().trim().equalsIgnoreCase("--") && disciplinaEntity.getSituacao().trim().equalsIgnoreCase("reprovado")) {
                disciplinaEntity.setMedia("0,0");
            }

            Double media = NumberUtils.decimalStringToDouble(disciplinaEntity.getMedia());

            aproveitamento = aproveitamento + NumberUtils.decimalStringToInt(disciplinaEntity.getMedia());

            if (disciplinaEntity.getMedia().trim().equalsIgnoreCase("--")) {
                disciplinaEntity.setMediaColor(R.color.color_008);
            } else {
                semetreCursado = true;
                disciplinaEntity.setMediaColor(getColorByScore(media));
            }
        }

        int totalAproveitamento = (aproveitamento / historicoSemestralSemestreEntity.getDisciplinas().size());

        if (semetreCursado) {
            historicoSemestralSemestreEntity.setAproveitamentoColor(getColorByAproveitamento(totalAproveitamento));
        } else {
            historicoSemestralSemestreEntity.setAproveitamentoColor(R.color.color_008);
        }

        historicoSemestralSemestreEntity.setAproveitamento(totalAproveitamento);
    }

    private int getColorByAproveitamento(int value) {
        int color = R.color.color_008;

        if (value >= 70) {
            color = R.color.color_012;
        }

        if (value < 50) {
            color = R.color.color_011;
        }

        return color;
    }

    private int getColorByScore(Double score) {
        int color = R.color.color_008;

        if (score >= 7.0f) {
            color = R.color.color_012;
        }

        if (score < 5.0f) {
            color = R.color.color_011;
        }

        return color;
    }
}
