package com.mobileclass.unipplus.modules.historicopagamentos;

import com.mobileclass.unipplus.modules.historicopagamentos.entities.HistoricoPagamentosEntity;
import com.mobileclass.unipplus.modules.historicopagamentos.interactor.HistoricoPagamentosInteractor;
import com.mobileclass.unipplus.modules.historicopagamentos.interactor.HistoricoPagamentosInteractorCallBack;

public class HistoricoPagamentosPresenter implements HIstoricoPagamentosContract.Presenter, HistoricoPagamentosInteractorCallBack {
    private HIstoricoPagamentosContract.View view;
    private HistoricoPagamentosInteractor interactor;

    public HistoricoPagamentosPresenter(HIstoricoPagamentosContract.View view) {
        this.view = view;
        interactor = new HistoricoPagamentosInteractor();
        interactor.setCallBack(this);
    }

    @Override
    public void start() {
        view.showLoading(true);
        interactor.getHistoricoPagamentos();
    }

    //INTERACTOR CALLBACK METHODS
    @Override
    public void onSuccess(HistoricoPagamentosEntity historicoPagamentosEntity) {
        view.setTableViewData(historicoPagamentosEntity.getGroupedHistoricoPagamentos());
    }

    @Override
    public void onFailure(String errorMessage) {
        view.showAlertDialog("", errorMessage);
    }

    @Override
    public void totalBills(String totalBills) {
        view.setTotalBills(totalBills);
    }
}
