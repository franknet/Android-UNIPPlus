package com.mobileclass.unipplus.modules.notasfaltas.interactor;

import com.mobileclass.unipplus.R;
import com.mobileclass.unipplus.UNIPPlusApplication;
import com.mobileclass.unipplus.modules.notasfaltas.NotasFaltasProvider;
import com.mobileclass.unipplus.modules.notasfaltas.entities.NotasFaltasDisciplinaEntity;
import com.mobileclass.unipplus.modules.notasfaltas.entities.NotasFaltasEntity;
import com.mobileclass.unipplus.modules.notasfaltas.entities.NotasFaltasListEntity;
import com.mobileclass.unipplus.net.LinksUNIP;
import com.mobileclass.unipplus.net.http.HttpClient;
import com.mobileclass.unipplus.utils.NumberUtils;
import com.mobileclass.unipplus.utils.TextUtils;

import java.util.ArrayList;
import java.util.List;

public class NotasFaltasInteractor {
    private static final String INTERNAL_ERROR = "Ocorreu um erro iniesperado, por favor tente novamente.";
    private NotasFaltasInteractorCallBack mCallBack;

    //PRIVATE PUBLIC METHOS
    public void setCallBack(NotasFaltasInteractorCallBack callBack) {
        mCallBack = callBack;
    }

    public void getNotasFaltas() {
        NotasFaltasProvider provider = new NotasFaltasProvider();

        HttpClient client = new HttpClient();
        client.doRequest(provider);
        client.result(new HttpClient.Result() {
            @Override
            public void onSuccess(int statusCode, byte[] data) {
                if (statusCode == 200) {
                    createNotasFaltasEntity(data);
                } else {
                    mCallBack.onFailure("Sessão expirada, logue-se novamente");
                }
            }

            @Override
            public void onFailure(String message) {
                mCallBack.onFailure(message);
            }
        });
        client.execute();
    }

    //PRIVATE METHODS
    private void createNotasFaltasEntity(byte[] data) {
        NotasFaltasListEntity notasFaltasListEntity = NotasFaltasParser.parse(data);
        validadeNotasFaltasEntity(notasFaltasListEntity);
    }

    private void validadeNotasFaltasEntity(NotasFaltasListEntity notasFaltasListEntity) {
        if (notasFaltasListEntity != null) {
            if (notasFaltasListEntity.getDisciplinas().isEmpty()) {
                mCallBack.onFailure("Relação de disciplinas não encontrada.");
            } else {
                List<List<NotasFaltasDisciplinaEntity>> groupedDisciplinas = groupDisciplinasByType(notasFaltasListEntity.getDisciplinas());
                NotasFaltasEntity notasFaltasEntity = new NotasFaltasEntity();
                notasFaltasEntity.setGroupedNotasFaltasList(groupedDisciplinas);

                UNIPPlusApplication.getInstance().getTemp().setNotasFaltasEntity(notasFaltasEntity);

                mCallBack.onSuccess(notasFaltasEntity);
            }
        } else {
            mCallBack.onFailure(INTERNAL_ERROR);
        }
    }

    private List<List<NotasFaltasDisciplinaEntity>> groupDisciplinasByType(List<NotasFaltasDisciplinaEntity> disciplinas) {
        List<List<NotasFaltasDisciplinaEntity>> groupedDisciplinas = new ArrayList<>();
        for (int i = 0; i < disciplinas.size(); i++) {
            NotasFaltasDisciplinaEntity disciplinaEntity = disciplinas.get(i);
            validadeStatus(disciplinaEntity);
            if (i == 0) {
                groupedDisciplinas.add(filterDisciplinas(disciplinas, disciplinaEntity.getTipodisciplina()));
            } else {
                NotasFaltasDisciplinaEntity previousDisciplinaEntity = disciplinas.get(i - 1);
                if (!disciplinaEntity.getTipodisciplina().equalsIgnoreCase(previousDisciplinaEntity.getTipodisciplina())) {
                    groupedDisciplinas.add(filterDisciplinas(disciplinas, disciplinaEntity.getTipodisciplina()));
                }
            }
        }
        return groupedDisciplinas;
    }

    private List<NotasFaltasDisciplinaEntity> filterDisciplinas(List<NotasFaltasDisciplinaEntity> disciplinas, String filter) {
        List<NotasFaltasDisciplinaEntity> filtredDisciplinas = new ArrayList<>();
        for (NotasFaltasDisciplinaEntity disciplinaEntity : disciplinas) {
            if (disciplinaEntity.getTipodisciplina().equalsIgnoreCase(filter)) {
                filtredDisciplinas.add(disciplinaEntity);
            }
        }
        return filtredDisciplinas;
    }

    private void validadeStatus(NotasFaltasDisciplinaEntity disciplinaEntity) {
        if (!disciplinaEntity.getNp1().equalsIgnoreCase("--") && !disciplinaEntity.getNp1().equalsIgnoreCase("nl")) {
            Double np1Double = NumberUtils.decimalStringToDouble(disciplinaEntity.getNp1());
            disciplinaEntity.setStatus(getStatusByScore(np1Double));
            disciplinaEntity.setNp1Color(getColorByScore(np1Double));
        } else {
            disciplinaEntity.setStatus("Aguardando NP1");
            disciplinaEntity.setNp1Color(R.color.color_008);
        }

        if (!disciplinaEntity.getNp2().equalsIgnoreCase("--") && !disciplinaEntity.getNp2().equalsIgnoreCase("nl")) {
            Double np2Double = NumberUtils.decimalStringToDouble(disciplinaEntity.getNp2());
            disciplinaEntity.setStatus("Verifique sua média semestral");
            disciplinaEntity.setNp2Color(getColorByScore(np2Double));
        } else {
            disciplinaEntity.setNp2Color(R.color.color_008);
        }
    }

    private String getStatusByScore(Double score) {
        Double calcNP2 = 14.0 - score;
        String statusStr;
        if (calcNP2 > 10.0f) {
            statusStr = "Verifique sua média semestral";
        } else if (calcNP2 <= 10.0f && calcNP2 >= 7.0f ) {
            statusStr = "Você precisa tirar <font color=#ee0000>" + TextUtils.doubleToString(calcNP2) + "</font> na NP2!";
        } else {
            statusStr = "Você precisa tirar <font color=#0000dd>" + TextUtils.doubleToString(calcNP2) + "</font> na NP2!";
        }
        return  statusStr;
    }

    private int getColorByScore(Double score) {
        int color = R.color.color_008;
        if (score >= 7.0f) {
            color = R.color.color_012;
        }
        if (score < 5.0f) {
            color = R.color.color_011;
        }
        return color;
    }
}
