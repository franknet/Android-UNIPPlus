package com.mobileclass.unipplus.modules.mediasexames.entities;

public class MediasExamesDisciplinaEntity {
    private String disciplina;
    private String exame;
    private int exameColor;
    private String faltas;
    private String mediafinal;
    private int mediaFinalColor;
    private String mediasemestral;
    private int mediaSemestralColor;
    private String nomedisciplina;
    private String obsdisciplina;
    private String tipodisciplina;
    private String turmaespecial;
    private String status;
    private int statusImage;

    public String getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }

    public String getExame() {
        return exame;
    }

    public void setExame(String exame) {
        this.exame = exame;
    }

    public int getExameColor() {
        return exameColor;
    }

    public void setExameColor(int exameColor) {
        this.exameColor = exameColor;
    }

    public String getFaltas() {
        return faltas;
    }

    public void setFaltas(String faltas) {
        this.faltas = faltas;
    }

    public String getMediafinal() {
        return mediafinal;
    }

    public void setMediafinal(String mediafinal) {
        this.mediafinal = mediafinal;
    }

    public int getMediaFinalColor() {
        return mediaFinalColor;
    }

    public void setMediaFinalColor(int mediaFinalColor) {
        this.mediaFinalColor = mediaFinalColor;
    }

    public String getMediasemestral() {
        return mediasemestral;
    }

    public void setMediasemestral(String mediasemestral) {
        this.mediasemestral = mediasemestral;
    }

    public int getMediaSemestralColor() {
        return mediaSemestralColor;
    }

    public void setMediaSemestralColor(int mediaSemestralColor) {
        this.mediaSemestralColor = mediaSemestralColor;
    }

    public String getNomedisciplina() {
        return nomedisciplina;
    }

    public void setNomedisciplina(String nomedisciplina) {
        this.nomedisciplina = nomedisciplina;
    }

    public String getObsdisciplina() {
        return obsdisciplina;
    }

    public void setObsdisciplina(String obsdisciplina) {
        this.obsdisciplina = obsdisciplina;
    }

    public String getTipodisciplina() {
        return tipodisciplina;
    }

    public void setTipodisciplina(String tipodisciplina) {
        this.tipodisciplina = tipodisciplina;
    }

    public String getTurmaespecial() {
        return turmaespecial;
    }

    public void setTurmaespecial(String turmaespecial) {
        this.turmaespecial = turmaespecial;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusImage() {
        return statusImage;
    }

    public void setStatusImage(int statusImage) {
        this.statusImage = statusImage;
    }
}
