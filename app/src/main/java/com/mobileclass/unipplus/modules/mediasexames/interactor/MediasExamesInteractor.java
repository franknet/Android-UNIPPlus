package com.mobileclass.unipplus.modules.mediasexames.interactor;

import com.mobileclass.unipplus.R;
import com.mobileclass.unipplus.UNIPPlusApplication;
import com.mobileclass.unipplus.modules.mediasexames.MediasExamesProvider;
import com.mobileclass.unipplus.modules.mediasexames.entities.MediasExamesDisciplinaEntity;
import com.mobileclass.unipplus.modules.mediasexames.entities.MediasExamesEntity;
import com.mobileclass.unipplus.modules.mediasexames.entities.MediasExamesListEntity;
import com.mobileclass.unipplus.modules.notasfaltas.entities.NotasFaltasDisciplinaEntity;
import com.mobileclass.unipplus.modules.notasfaltas.entities.NotasFaltasEntity;
import com.mobileclass.unipplus.modules.notasfaltas.entities.NotasFaltasListEntity;
import com.mobileclass.unipplus.net.LinksUNIP;
import com.mobileclass.unipplus.net.http.HttpClient;
import com.mobileclass.unipplus.utils.NumberUtils;
import com.mobileclass.unipplus.utils.TextUtils;

import java.util.ArrayList;
import java.util.List;

public class MediasExamesInteractor {
    private static final String INTERNAL_ERROR = "Ocorreu um erro iniesperado, por favor tente novamente.";
    private MediasExamesInteractorCallBack callBack;

    public void setCallBack(MediasExamesInteractorCallBack callBack) {
        this.callBack = callBack;
    }

    public void getMediasExames() {
        MediasExamesProvider provider = new MediasExamesProvider();

        HttpClient client = new HttpClient();
        client.doRequest(provider);
        client.result(new HttpClient.Result() {
            @Override
            public void onSuccess(int statusCode, byte[] data) {
                if (statusCode == 200) {
                    createMediasExamesEntity(data);
                } else {
                    callBack.onFailure("Sessão expirada, logue-se novamente");
                }
            }

            @Override
            public void onFailure(String message) {
                callBack.onFailure(message);
            }
        });
        client.execute();
    }

    private void createMediasExamesEntity(byte[] data) {
        MediasExamesListEntity mediasExamesListEntity = MediasExamesParser.parse(data);
        validadeMediasExamesEntity(mediasExamesListEntity);
    }

    private void validadeMediasExamesEntity(MediasExamesListEntity mediasExamesListEntity) {
        if (mediasExamesListEntity != null) {
            if (mediasExamesListEntity.getDisciplinas().isEmpty()) {
                callBack.onFailure("Relação de disciplinas não encontrada.");
            } else {
                List<List<MediasExamesDisciplinaEntity>> groupedMediasExamesDisciplinas = groupDisciplinasByType(mediasExamesListEntity.getDisciplinas());
                MediasExamesEntity mediasExamesEntity = new MediasExamesEntity();
                mediasExamesEntity.setGroupedMediasExamesList(groupedMediasExamesDisciplinas);
                callBack.onSuccess(mediasExamesEntity);
            }
        } else {
            callBack.onFailure(INTERNAL_ERROR);
        }
    }

    private List<List<MediasExamesDisciplinaEntity>> groupDisciplinasByType(List<MediasExamesDisciplinaEntity> disciplinas) {
        List<List<MediasExamesDisciplinaEntity>> groupedDisciplinas = new ArrayList<>();
        for (int i = 0; i < disciplinas.size(); i++) {
            MediasExamesDisciplinaEntity disciplinaEntity = disciplinas.get(i);
            validadeMediaSemestral(disciplinaEntity);
            validadeExame(disciplinaEntity);
            validadeMediaFinal(disciplinaEntity);
            validadeStatusForMS(disciplinaEntity);
            if (i == 0) {
                groupedDisciplinas.add(filterDisciplinas(disciplinas, disciplinaEntity.getTipodisciplina()));
            } else {
                MediasExamesDisciplinaEntity previousDisciplinaEntity = disciplinas.get(i - 1);
                if (!disciplinaEntity.getTipodisciplina().equalsIgnoreCase(previousDisciplinaEntity.getTipodisciplina())) {
                    groupedDisciplinas.add(filterDisciplinas(disciplinas, disciplinaEntity.getTipodisciplina()));
                }
            }
        }
        return groupedDisciplinas;
    }

    private List<MediasExamesDisciplinaEntity> filterDisciplinas(List<MediasExamesDisciplinaEntity> disciplinas, String filter) {
        List<MediasExamesDisciplinaEntity> filtredDisciplinas = new ArrayList<>();
        for (MediasExamesDisciplinaEntity disciplinaEntity : disciplinas) {
            if (disciplinaEntity.getTipodisciplina().equalsIgnoreCase(filter)) {
                filtredDisciplinas.add(disciplinaEntity);
            }
        }
        return filtredDisciplinas;
    }

    private void validadeMediaSemestral(MediasExamesDisciplinaEntity mediasExamesDisciplinaEntity) {
        NotasFaltasEntity notasFaltasEntity = UNIPPlusApplication.getInstance().getTemp().getNotasFaltasEntity();
        if (notasFaltasEntity != null) {
            NotasFaltasDisciplinaEntity disciplinaEntity = getNotasFaltasDisciplina(notasFaltasEntity.getGroupedNotasFaltasList(), mediasExamesDisciplinaEntity.getDisciplina());

            if (disciplinaEntity.getNp2().equalsIgnoreCase("nl")) {
                mediasExamesDisciplinaEntity.setMediasemestral("--");
            }
        }


        if (!mediasExamesDisciplinaEntity.getMediasemestral().equalsIgnoreCase("--") && !mediasExamesDisciplinaEntity.getMediasemestral().equalsIgnoreCase("nl")) {
            Double ms = NumberUtils.decimalStringToDouble(mediasExamesDisciplinaEntity.getMediasemestral());
            mediasExamesDisciplinaEntity.setMediaSemestralColor(getColorByScore(ms));
        } else {
            mediasExamesDisciplinaEntity.setMediaSemestralColor(R.color.color_008);
        }
    }

    private void validadeExame(MediasExamesDisciplinaEntity mediasExamesDisciplinaEntity) {
        if (!mediasExamesDisciplinaEntity.getExame().equalsIgnoreCase("--") && !mediasExamesDisciplinaEntity.getExame().equalsIgnoreCase("nl")) {
            Double ex = NumberUtils.decimalStringToDouble(mediasExamesDisciplinaEntity.getExame());
            mediasExamesDisciplinaEntity.setExameColor(getColorByScore(ex));
        } else {
            mediasExamesDisciplinaEntity.setExameColor(R.color.color_008);
        }
    }

    private void validadeMediaFinal(MediasExamesDisciplinaEntity mediasExamesDisciplinaEntity) {
        if (mediasExamesDisciplinaEntity.getMediasemestral().equalsIgnoreCase("--")) {
            mediasExamesDisciplinaEntity.setMediafinal(mediasExamesDisciplinaEntity.getMediasemestral());
        }
        if (mediasExamesDisciplinaEntity.getMediafinal().equalsIgnoreCase("--")) {
            mediasExamesDisciplinaEntity.setMediaFinalColor(R.color.color_008);
        } else {
            Double mf = NumberUtils.decimalStringToDouble(mediasExamesDisciplinaEntity.getMediafinal());
            mediasExamesDisciplinaEntity.setMediaFinalColor(getColorByScore(mf));
        }
    }

    private void validadeStatusForMS(MediasExamesDisciplinaEntity mediasExamesDisciplinaEntity) {
        if (mediasExamesDisciplinaEntity.getMediasemestral().equalsIgnoreCase("--") || mediasExamesDisciplinaEntity.getMediasemestral().equalsIgnoreCase("nl")) {
            mediasExamesDisciplinaEntity.setStatus("Aguardando média semestral.");
        } else if (mediasExamesDisciplinaEntity.getMediasemestral().equalsIgnoreCase("re")) {
            mediasExamesDisciplinaEntity.setStatusImage(R.mipmap.error_icon);
            mediasExamesDisciplinaEntity.setStatus("Você foi reprovado!");
        } else {
            Double ms = NumberUtils.decimalStringToDouble(mediasExamesDisciplinaEntity.getMediasemestral());
            if (ms < 7.0f) {
                validadeStatusForEX(mediasExamesDisciplinaEntity);
            } else {
                mediasExamesDisciplinaEntity.setStatusImage(R.mipmap.success_icon);
                mediasExamesDisciplinaEntity.setStatus("Você foi aprovado!");
            }
        }
    }

    private void validadeStatusForEX(MediasExamesDisciplinaEntity mediasExamesDisciplinaEntity) {
        if (mediasExamesDisciplinaEntity.getExame().equalsIgnoreCase("--") || mediasExamesDisciplinaEntity.getExame().equalsIgnoreCase("nl")) {
            Double ms = NumberUtils.decimalStringToDouble(mediasExamesDisciplinaEntity.getMediasemestral());
            Double calcEX = 10.0 - ms;
            mediasExamesDisciplinaEntity.setStatus("Você precisa tirar " + TextUtils.doubleToString(calcEX) + " no exame!");
        } else {
            validadeStatusForMF(mediasExamesDisciplinaEntity);
        }
    }

    private void validadeStatusForMF(MediasExamesDisciplinaEntity mediasExamesDisciplinaEntity) {
        Double mf = NumberUtils.decimalStringToDouble(mediasExamesDisciplinaEntity.getMediafinal());
        if (mf < 5.0) {
            mediasExamesDisciplinaEntity.setStatusImage(R.mipmap.error_icon);
            mediasExamesDisciplinaEntity.setStatus("Você foi reprovado!");
        } else {
            mediasExamesDisciplinaEntity.setStatusImage(R.mipmap.success_icon);
            mediasExamesDisciplinaEntity.setStatus("Você foi aprovado!");
        }
    }

    private int getColorByScore(Double score) {
        int color = R.color.color_008;
        if (score >= 7.0f) {
            color = R.color.color_012;
        }
        if (score < 5.0f) {
            color = R.color.color_011;
        }
        return color;
    }

    private NotasFaltasDisciplinaEntity getNotasFaltasDisciplina(List<List<NotasFaltasDisciplinaEntity>> groupNotasFaltasDisciplinaEntities, String disciplina) {
        NotasFaltasDisciplinaEntity notasFaltasDisciplinaEntity = null;
        boolean match = false;

        for (List<NotasFaltasDisciplinaEntity> notasFaltasDisciplinaEntities : groupNotasFaltasDisciplinaEntities) {
            if (match) {
                break;
            } else {
                for (NotasFaltasDisciplinaEntity notasFaltasDisciplinaEntity1 : notasFaltasDisciplinaEntities) {
                    if (notasFaltasDisciplinaEntity1.getDisciplina().equalsIgnoreCase(disciplina)) {
                        notasFaltasDisciplinaEntity = notasFaltasDisciplinaEntity1;
                        match = true;
                        break;
                    }
                }
            }
        }
        return notasFaltasDisciplinaEntity;
    }
}
