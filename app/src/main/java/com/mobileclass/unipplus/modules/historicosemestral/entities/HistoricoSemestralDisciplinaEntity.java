package com.mobileclass.unipplus.modules.historicosemestral.entities;

public class HistoricoSemestralDisciplinaEntity {
    private String semestre;
    private String codigo;
    private String nomedisciplina;
    private String cargahoraria;
    private String ano;
    private String media;
    private int mediaColor;
    private String situacao;

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNomedisciplina() {
        return nomedisciplina;
    }

    public void setNomedisciplina(String nomedisciplina) {
        this.nomedisciplina = nomedisciplina;
    }

    public String getCargahoraria() {
        return cargahoraria;
    }

    public void setCargahoraria(String cargahoraria) {
        this.cargahoraria = cargahoraria;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public int getMediaColor() {
        return mediaColor;
    }

    public void setMediaColor(int mediaColor) {
        this.mediaColor = mediaColor;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }
}
