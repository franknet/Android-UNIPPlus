package com.mobileclass.unipplus.modules.historicosemestral.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mobileclass.unipplus.R;
import com.mobileclass.unipplus.customviews.CircularProgressbar;
import com.mobileclass.unipplus.modules.historicosemestral.entities.HistoricoSemestralSemestreEntity;
import com.mobileclass.unipplus.services.google.admob.AdMobService;
import com.mobileclass.unipplus.services.google.admob.BannerAdListener;
import com.mobileclass.unipplus.utils.NumberUtils;

import java.util.List;

public class HistoricoSemestralBaseExpandableAdapter extends BaseExpandableListAdapter {
    private LayoutInflater layoutInflater;
    private List<HistoricoSemestralSemestreEntity> historicoSemestralSemestreEntities;
    
    public HistoricoSemestralBaseExpandableAdapter(List<HistoricoSemestralSemestreEntity> historicoSemestralSemestreEntities, Context context) {
        this.historicoSemestralSemestreEntities = historicoSemestralSemestreEntities;
        this.layoutInflater = LayoutInflater.from(context);
    }
    
    @Override
    public int getGroupCount() {
        return historicoSemestralSemestreEntities.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return historicoSemestralSemestreEntities.get(i).getDisciplinas().size();
    }

    @Override
    public Object getGroup(int i) {
        return null;
    }

    @Override
    public Object getChild(int i, int i1) {
        return null;
    }

    @Override
    public long getGroupId(int i) {
        return 0;
    }

    @Override
    public long getChildId(int i, int i1) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder                                  = new ViewHolder();
            convertView                                 = this.layoutInflater.inflate(R.layout.list_group_item_historico_semestral, parent, false);

            viewHolder.txvGroupTitle                    = (TextView) convertView.findViewById(R.id.txvGroupTitle);
            viewHolder.txvHistSemestralAVG              = (TextView) convertView.findViewById(R.id.txvHistSemestralAVG);
            viewHolder.cpbHistSemestralAVG              = (CircularProgressbar) convertView.findViewById(R.id.cpbHistSemestralAVG);

            convertView.setTag(viewHolder);
        } else {
            viewHolder                                  = (ViewHolder) convertView.getTag();
        }

        int aproveitamentoColor = convertView.getContext().getResources().getColor(historicoSemestralSemestreEntities.get(i).getAproveitamentoColor());

        viewHolder.txvGroupTitle.setText(historicoSemestralSemestreEntities.get(i).getSemestre());
        viewHolder.txvHistSemestralAVG.setText(String.valueOf(historicoSemestralSemestreEntities.get(i).getAproveitamento()) + "%");
        viewHolder.cpbHistSemestralAVG.setProgress(historicoSemestralSemestreEntities.get(i).getAproveitamento());
        viewHolder.cpbHistSemestralAVG.setColor(aproveitamentoColor);

        return convertView;
    }

    @Override
    public View getChildView(int groupIndex, int childIndex, boolean b, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder                                  = new ViewHolder();
            convertView                                 = this.layoutInflater.inflate(R.layout.list_child_item_historico_semestral, parent, false);

            viewHolder.txvHistSemestralDisciplina       = (TextView) convertView.findViewById(R.id.txvHistSemestralDisciplina);
            viewHolder.txvHistSemestralCH               = (TextView) convertView.findViewById(R.id.txvHistSemestralCH);
            viewHolder.txvHistSemestralME               = (TextView) convertView.findViewById(R.id.txvHistSemestralME);
            viewHolder.txvHistSemestralAno              = (TextView) convertView.findViewById(R.id.txvHistSemestralAno);
            viewHolder.txvHistSemestralSituacao         = (TextView) convertView.findViewById(R.id.txvHistSemestralSituacao);
            viewHolder.cpbHistSemestralME               = (CircularProgressbar) convertView.findViewById(R.id.cpbHistSemestralME);
            viewHolder.advBanner                        = (AdView) convertView.findViewById(R.id.advBanner);

            convertView.setTag(viewHolder);
        } else {
            viewHolder                                  = (ViewHolder) convertView.getTag();
        }

        int meColor = convertView.getContext().getResources().getColor(historicoSemestralSemestreEntities.get(groupIndex).getDisciplinas().get(childIndex).getMediaColor());

        int bannerIndex = historicoSemestralSemestreEntities.get(groupIndex).getDisciplinas().size() / 2;
        if (childIndex == bannerIndex) {
            AdMobService.requestBannerAd(viewHolder.advBanner, new BannerAdListener() {
                @Override
                public void onAdLoadFinished(AdView adView) {
                    adView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAdLoadFinishedWithError(AdView adView, String message) {
                }
            });
        } else {
            viewHolder.advBanner.setVisibility(View.GONE);
        }

        viewHolder.txvHistSemestralDisciplina.setText(historicoSemestralSemestreEntities.get(groupIndex).getDisciplinas().get(childIndex).getNomedisciplina());
        viewHolder.txvHistSemestralCH.setText("CH: " + historicoSemestralSemestreEntities.get(groupIndex).getDisciplinas().get(childIndex).getCargahoraria());
        viewHolder.txvHistSemestralME.setText(String.valueOf(NumberUtils.decimalStringToDouble(historicoSemestralSemestreEntities.get(groupIndex).getDisciplinas().get(childIndex).getMedia())));
        viewHolder.txvHistSemestralME.setTextColor(meColor);
        viewHolder.cpbHistSemestralME.setColor(meColor);
        viewHolder.cpbHistSemestralME.setProgress(NumberUtils.decimalStringToInt(historicoSemestralSemestreEntities.get(groupIndex).getDisciplinas().get(childIndex).getMedia()));
        viewHolder.txvHistSemestralAno.setText("Ano: " + historicoSemestralSemestreEntities.get(groupIndex).getDisciplinas().get(childIndex).getAno());
        viewHolder.txvHistSemestralSituacao.setText(historicoSemestralSemestreEntities.get(groupIndex).getDisciplinas().get(childIndex).getSituacao());

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }

    private class ViewHolder {
        AdView advBanner;
        TextView txvHistSemestralDisciplina;
        TextView txvHistSemestralCH;
        TextView txvHistSemestralME;
        TextView txvHistSemestralAno;
        TextView txvGroupTitle;
        TextView txvHistSemestralSituacao;
        TextView txvHistSemestralAVG;
        CircularProgressbar cpbHistSemestralAVG;
        CircularProgressbar cpbHistSemestralME;
    }
}
