package com.mobileclass.unipplus.modules.login.interactor;

import com.google.firebase.remoteconfig.FirebaseRemoteConfigValue;
import com.google.gson.Gson;
import com.mobileclass.unipplus.UNIPPlusApplication;
import com.mobileclass.unipplus.modules.login.providers.LoginAuthenticationProvider;
import com.mobileclass.unipplus.modules.login.providers.LoginSessionProvider;
import com.mobileclass.unipplus.modules.login.entity.LoginResponseEntity;
import com.mobileclass.unipplus.modules.login.entity.UserEntity;
import com.mobileclass.unipplus.net.http.HttpClient;
import com.mobileclass.unipplus.services.google.RemoteConfig;
import com.mobileclass.unipplus.services.google.RemoteDatabase;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class LoginInteractor {
    private LoginInteractorCallback mCallback;
    private boolean mRecordCredentials = true;

    // PUBLIC METHODS
    public void setCallback(LoginInteractorCallback callback) {
        mCallback = callback;
    }

    public void verifyNewVersion() {
        Map<String, FirebaseRemoteConfigValue> config = RemoteConfig.shared().getConfiguration();
        if (config != null) {
            try {
                long remoteAppVersion = Objects.requireNonNull(config.get("current_android_version")).asLong();
                mRecordCredentials = Objects.requireNonNull(config.get("record_credentials")).asBoolean();
                long appVersionCode = UNIPPlusApplication.getInstance().getInfo().getAppVersionCode();
                mCallback.onVerifyNewVersion(remoteAppVersion != appVersionCode);
            } catch (NullPointerException ignored) { }
        }
    }

    public void doLogin(final UserEntity userEntity) {
        LoginAuthenticationProvider provider = new LoginAuthenticationProvider(userEntity);

        HttpClient client = new HttpClient();
        client.doRequest(provider);
        client.result(new HttpClient.Result() {
            @Override
            public void onSuccess(int statusCode, byte[] data) {
                String json = new String(data);
                final LoginResponseEntity result = new Gson().fromJson(json, LoginResponseEntity.class);
                if (result.valida) {
                    if (mRecordCredentials) {
                        Map<String,Object> credentials = new HashMap<>();
                        credentials.put(result.identificacao, userEntity.getPassword());
                        RemoteDatabase.saveData(credentials);
                    }
                    if (Objects.equals(result.instituto, "UNIP")) {
                        mCallback.onLoginAuthenticatedWithSuccess(result);
                    } else {
                        mCallback.onLoginAuthenticatedWithFailure("No momento o app não suporta cursos a distância");
                    }
                } else {
                    mCallback.onLoginAuthenticatedWithFailure(result.mensagemInvalida);
                }
            }

            @Override
            public void onFailure(String message) {
                mCallback.onLoginAuthenticatedWithFailure(message);
            }
        });
        client.execute();
    }

    // PRIVATE METHODS
    public void initSession(String token) {
        LoginSessionProvider provider = new LoginSessionProvider(token);

        HttpClient client = new HttpClient();
        client.doRequest(provider);
        client.result(new HttpClient.Result() {
            @Override
            public void onSuccess(int statusCode, byte[] data) {
                if (statusCode == 302) {
                    mCallback.onSessionInitializedWithSuccess();
                } else {
                    mCallback.onSessionInitializedWithFailure("Algo deu errado, tente novamente.");
                }
            }

            @Override
            public void onFailure(String message) {
                mCallback.onSessionInitializedWithFailure(message);
            }
        });
        client.execute();
    }
}
