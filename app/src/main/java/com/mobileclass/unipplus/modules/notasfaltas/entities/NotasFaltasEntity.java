package com.mobileclass.unipplus.modules.notasfaltas.entities;

import java.util.List;

public class NotasFaltasEntity {
    private List<List<NotasFaltasDisciplinaEntity>> mGroupedDisciplinaEntities;

    public List<List<NotasFaltasDisciplinaEntity>> getGroupedNotasFaltasList() {
        return mGroupedDisciplinaEntities;
    }

    public void setGroupedNotasFaltasList(List<List<NotasFaltasDisciplinaEntity>> groupedDisciplinaEntities) {
        mGroupedDisciplinaEntities = groupedDisciplinaEntities;
    }
}
