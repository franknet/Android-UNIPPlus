package com.mobileclass.unipplus.modules.notasfaltas.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.mobileclass.unipplus.R;
import com.mobileclass.unipplus.bases.BaseFragment;
import com.mobileclass.unipplus.modules.notasfaltas.NotasFaltasContract;
import com.mobileclass.unipplus.modules.notasfaltas.entities.NotasFaltasDisciplinaEntity;
import com.mobileclass.unipplus.modules.notasfaltas.NotasFaltasPresenter;

import java.util.List;

public class NotasFaltasFragment extends BaseFragment implements NotasFaltasContract.View {
    private TextView txvErrorMessage;
    private ExpandableListView elvNotasFaltas;
    private ViewFlipper viewFlipper;
    private NotasFaltasPresenter presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);
        this.presenter = new NotasFaltasPresenter(this);
        this.viewFlipper = (ViewFlipper) inflater.inflate(R.layout.fragment_notas_faltas, container, false);
        this.elvNotasFaltas = viewFlipper.findViewById(R.id.elvNotasFaltas);
        this.txvErrorMessage = viewFlipper.findViewById(R.id.txvErrorMessage);

        Button btnTryAgain = viewFlipper.findViewById(R.id.btnTryAgain);
        btnTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.start();
            }
        });

        return this.viewFlipper;
    }

    //VIEW METHODS
    @Override
    public void onResume() {
        super.onResume();
        this.presenter.start();
    }

    @Override
    public void showLoading(boolean show) {
        this.viewFlipper.setDisplayedChild(0);
    }

    @Override
    public void hideKeyboard() {
    }

    @Override
    public void showAlertDialog(String title, String message) {
        this.txvErrorMessage.setText(message);
        this.viewFlipper.setDisplayedChild(2);
    }

    @Override
    public void setTableViewData(List<List<NotasFaltasDisciplinaEntity>> groupedDisciplinaEntities) {
        this.viewFlipper.setDisplayedChild(1);
        NotasFaltasBaseExpandableAdapter adapter = new NotasFaltasBaseExpandableAdapter(groupedDisciplinaEntities, getContext());
        this.elvNotasFaltas.setAdapter(adapter);
        expandListView(adapter);
    }

    @Override
    public boolean isRunningInBackground() {
        return super.isRunningInBackground();
    }

    private void expandListView(BaseExpandableListAdapter adapter) {
        int index = 0;
        while (index < adapter.getGroupCount()) {
            this.elvNotasFaltas.expandGroup(index);
            index++;
        }
    }
}
