package com.mobileclass.unipplus.modules.onlineclasses.interactor;

import com.mobileclass.unipplus.modules.onlineclasses.entities.OnlineClassEntity;
import com.mobileclass.unipplus.modules.onlineclasses.entities.OnlineClassZoomEntity;

import java.util.List;

public interface OnlineClassesInteractorCallBack {
    void onLogClassSuccess(OnlineClassZoomEntity zoom);
    void onSuccess(List<OnlineClassEntity> classes);
    void onFailure(String errorMessage);
}
