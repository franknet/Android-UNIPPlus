package com.mobileclass.unipplus.modules.login.entity;

public class AttributesEntity {
    private String name;
    private String value;
    private String src;
    private String type;

    public AttributesEntity(String csv) {
        String[] attrs = csv.split("\\|");
        this.name = attrs[0];
        this.value = attrs[1];
        this.src = attrs[2];
        this.type = attrs[3];
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public String getSrc() {
        return src;
    }

    public String getType() {
        return type;
    }
}
