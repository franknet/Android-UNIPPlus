package com.mobileclass.unipplus.modules.home.interactor;

import androidx.fragment.app.Fragment;

import java.util.List;

/**
 * Created by hackintosh on 19/03/2017.
 */

public interface ContentInteractorCallBack {
    void onContentsLoaded(List<Fragment> contents);
    void onContentsTitlesLoaded(String[] titles);
}
