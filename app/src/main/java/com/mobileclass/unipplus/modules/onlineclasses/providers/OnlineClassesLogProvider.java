package com.mobileclass.unipplus.modules.onlineclasses.providers;

import com.mobileclass.unipplus.UNIPPlusApplication;
import com.mobileclass.unipplus.modules.login.entity.UserEntity;
import com.mobileclass.unipplus.modules.onlineclasses.entities.OnlineClassZoomEntity;
import com.mobileclass.unipplus.net.http.HttpClient;
import com.mobileclass.unipplus.net.http.HttpProvider;

import okhttp3.FormBody;
import okhttp3.RequestBody;

public class OnlineClassesLogProvider extends HttpProvider {
    private final OnlineClassZoomEntity zoom;

    public OnlineClassesLogProvider(OnlineClassZoomEntity zoom) {
        this.zoom = zoom;
    }

    @Override
    public String path() {
        return "https://sec1.unip.br/SecrOnlineNew/AulaOnline/LogAluno";
    }

    @Override
    public HttpClient.RequestMethod method() {
        return HttpClient.RequestMethod.Post;
    }

    @Override
    public RequestBody body() {
        UserEntity userEntity = UNIPPlusApplication.getInstance().getCache().getUserEntity();

        FormBody.Builder builder = new FormBody.Builder();
        builder.add("CodDIsc", zoom.getCodDIsc());
        builder.add("CodCurso", zoom.getCodCurso());
        builder.add("Turma", zoom.getTurma());
        builder.add("LinkAula", zoom.getLinkAula());
        builder.add("NomeDisc", zoom.getNomeDisc());
        builder.add("HoraEntrada", zoom.getHoraEntrada());
        builder.add("MatriculaLog", userEntity.getRa().toUpperCase());
        builder.add("DataInicio", zoom.getDataInicio());
        builder.add("DataFim", zoom.getDataFim());

        return builder.build();
    }
}
