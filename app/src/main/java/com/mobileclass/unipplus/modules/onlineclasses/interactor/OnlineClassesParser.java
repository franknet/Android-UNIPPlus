package com.mobileclass.unipplus.modules.onlineclasses.interactor;

import android.app.Application;

import com.mobileclass.unipplus.modules.onlineclasses.entities.OnlineClassEntity;
import com.mobileclass.unipplus.modules.onlineclasses.entities.OnlineClassZoomEntity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class OnlineClassesParser {

    public static List<OnlineClassEntity> create(byte[] data) {
        try {
            String html = new String(data);
            ArrayList<OnlineClassEntity> onlineClasses = new ArrayList<>();

            Document document = Jsoup.parse(html);
            Element table = document.getElementsByClass("table-striped").first();
            Elements trs = table.getElementsByTag("tr");
            trs.remove(0);

            for (Element tr : trs) {
                OnlineClassEntity onlineClass = new OnlineClassEntity();

                String tdString = tr.getElementsByTag("td").get(0).text().trim();
                onlineClass.setDiscipline(tdString);
                tdString = tr.getElementsByTag("td").get(1).text().trim();
                onlineClass.setDatetime(tdString);
                tdString = tr.getElementsByTag("td").get(2).text().trim();
                onlineClass.setStatus(tdString);
                onlineClass.setZoom(parseZoom(tr.getElementsByTag("td").get(2).attr("onclick")));
                onlineClass.setStatusCode(checkStatusCode(tr.getElementsByTag("td").get(2).attr("style")));

                onlineClasses.add(onlineClass);
            }
            return onlineClasses;
        } catch (Exception e) {
            return null;
        }
    }

    private static OnlineClassZoomEntity parseZoom(String onclick) {
        OnlineClassZoomEntity zoom = null;
        String[] attrs = onclick.split("'");
        if (attrs.length >= 15 ) {
            zoom = new OnlineClassZoomEntity();
            zoom.setCodDIsc(attrs[1]);
            zoom.setCodCurso(attrs[3]);
            zoom.setTurma(attrs[5]);
            zoom.setLinkAula(attrs[7]);
            zoom.setNomeDisc(attrs[9]);
            zoom.setHoraEntrada(attrs[11]);
            zoom.setDataInicio(attrs[13]);
            zoom.setDataFim(attrs[15]);
        }
        return zoom;
    }

    private static OnlineClassEntity.StatusCode checkStatusCode(String style) {
        if (style.equals("color:red")) {
            return OnlineClassEntity.StatusCode.HAS_PASSED;
        } else if (style.equals("color:green")) {
            return OnlineClassEntity.StatusCode.ON_LIVE;
        } else {
            return OnlineClassEntity.StatusCode.PENDING;
        }
    }
}
