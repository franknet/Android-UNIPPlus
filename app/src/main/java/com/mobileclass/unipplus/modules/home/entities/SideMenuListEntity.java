package com.mobileclass.unipplus.modules.home.entities;

import android.graphics.drawable.Drawable;

import java.util.ArrayList;

public class SideMenuListEntity {
    public final static int BIBLIOTECA_ID       = 1;
    public final static int HORARIO_AULAS_ID    = 2;
    public final static int AVISOS_ID           = 3;
    public final static int COMPARTILHAR_ID     = 4;
    public final static int AVALIAR_ID          = 5;

    private int id;
    private int icon;
    private String title;
    private ArrayList<Child> children;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<Child> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<Child> children) {
        this.children = children;
    }

    public static class Child {
        private Drawable icon;
        private String title;

        public Drawable getIcon() {
            return icon;
        }

        public void setIcon(Drawable icon) {
            this.icon = icon;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
