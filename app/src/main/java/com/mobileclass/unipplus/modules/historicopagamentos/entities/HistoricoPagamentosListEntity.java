package com.mobileclass.unipplus.modules.historicopagamentos.entities;

import java.util.ArrayList;

public class HistoricoPagamentosListEntity {
    private ArrayList<HistoricoPagamentosPosicaoEntity> posicao;
    private String erro;

    public ArrayList<HistoricoPagamentosPosicaoEntity> getPosicao() {
        return posicao;
    }

    public void setPosicao(ArrayList<HistoricoPagamentosPosicaoEntity> posicao) {
        this.posicao = posicao;
    }

    public String getErro() {
        return erro;
    }
}
