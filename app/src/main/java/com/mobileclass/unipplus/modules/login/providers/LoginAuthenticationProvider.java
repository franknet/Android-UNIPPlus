package com.mobileclass.unipplus.modules.login.providers;

import com.google.gson.JsonObject;
import com.mobileclass.unipplus.modules.login.entity.UserEntity;
import com.mobileclass.unipplus.net.LinksUNIP;
import com.mobileclass.unipplus.net.http.HttpClient;
import com.mobileclass.unipplus.net.http.HttpProvider;
import com.mobileclass.unipplus.security.EncryptManager;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class LoginAuthenticationProvider extends HttpProvider {
    private final UserEntity mUserEntity;

    public LoginAuthenticationProvider(UserEntity userEntity) {
        mUserEntity = userEntity;
    }

    @Override
    public String path() {
        return LinksUNIP.URL_LOGIN;
    }

    @Override
    public HttpClient.RequestMethod method() {
        return HttpClient.RequestMethod.Post;
    }

    @Override
    public RequestBody body() {
        MediaType type = MediaType.parse("application/json; charset=utf-8");
        JsonObject json = new JsonObject();
        json.addProperty("identificacao", mUserEntity.getRa());
        json.addProperty("senha", mUserEntity.getPassword());
        return RequestBody.create(type, json.toString());
    }
}
