package com.mobileclass.unipplus.modules.login.interactor;

import com.mobileclass.unipplus.modules.login.entity.LoginResponseEntity;

public interface LoginInteractorCallback {
    void onLoginAuthenticatedWithSuccess(LoginResponseEntity result);
    void onLoginAuthenticatedWithFailure(String message);
    void onSessionInitializedWithSuccess();
    void onSessionInitializedWithFailure(String message);
    void onVerifyNewVersion(boolean showNewVersionAlert);
}
