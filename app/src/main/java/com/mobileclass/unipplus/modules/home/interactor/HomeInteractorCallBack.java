package com.mobileclass.unipplus.modules.home.interactor;

public interface HomeInteractorCallBack {
    void mustRate(String message);
}
