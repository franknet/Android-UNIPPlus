package com.mobileclass.unipplus.modules.home.presenter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;

import com.mobileclass.unipplus.modules.home.entities.SideMenuListEntity;
import com.mobileclass.unipplus.modules.home.interactor.SideMenuInteractor;
import com.mobileclass.unipplus.modules.home.interactor.SideMenuInteractorCallBack;
import com.mobileclass.unipplus.modules.home.view.fragments.SideMenuView;

import java.util.ArrayList;

public class SideMenuPresenter implements SideMenuInteractorCallBack {
    private SideMenuView view;

    public SideMenuPresenter(SideMenuView view) {
        this.view = view;
        SideMenuInteractor interactor = new SideMenuInteractor();
        interactor.setCallBack(this);
        interactor.loadSideMenuContents();
    }

    public void groupSelected(int id) {
       if (id == SideMenuListEntity.COMPARTILHAR_ID) {
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("text/plain");
            share.putExtra(Intent.EXTRA_TEXT,
                            "Baixe o aplicativo UNIP Plus!\n\n" +
                            "Android\n" +
                            "https://play.google.com/store/apps/details?id=com.mobileclass.unipplus");
            this.view.goTo(Intent.createChooser(share, "Compartilhar UNIP Plus com:"));
        }

        if (id == SideMenuListEntity.AVALIAR_ID) {
            Uri uri = Uri.parse("market://details?id=com.mobileclass.unipplus");
            Intent googlePlay = new Intent(Intent.ACTION_VIEW, uri);
            googlePlay.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.view.goTo(googlePlay);
        }

        if (id == SideMenuListEntity.BIBLIOTECA_ID) {
            view.startLibrayActivity();
        }
    }

    @Override
    public void onContentsLoaded(ArrayList<SideMenuListEntity> sideMenuListEntities) {
        this.view.setMenuContents(sideMenuListEntities);
    }

    @Override
    public void onUserImageLoaded(Bitmap userImage) {
        if (userImage != null) {
            view.setUserProfile(userImage);
        }
    }
}
