package com.mobileclass.unipplus.modules.login.entity;

public class UserEntity {
    private String name;
    private String ra;
    private String password;
    private String captcha;
    private boolean keepCredentials;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRa() {
        return ra;
    }

    public void setRa(String ra) {
        this.ra = ra;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isKeepCredentials() {
        return keepCredentials;
    }

    public void setKeepCredentials(boolean keepCredentials) {
        this.keepCredentials = keepCredentials;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }
}
