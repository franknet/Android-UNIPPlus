package com.mobileclass.unipplus.modules.historicosemestral;

import com.mobileclass.unipplus.modules.historicosemestral.entities.HistoricoSemestralEntity;
import com.mobileclass.unipplus.modules.historicosemestral.interactor.HistoricoSemestralInteractorCallBack;
import com.mobileclass.unipplus.modules.historicosemestral.interactor.HistoricoSemestralInteractor;

public class HistoricoSemestralPresenter implements HistoricoSemestralContract.Presenter, HistoricoSemestralInteractorCallBack {
    private HistoricoSemestralContract.View view;
    private HistoricoSemestralInteractor interactor;

    public HistoricoSemestralPresenter(HistoricoSemestralContract.View view) {
        this.view = view;
        interactor = new HistoricoSemestralInteractor();
        interactor.setCallBack(this);
    }

    @Override
    public void start() {
        this.view.showLoading(true);
        interactor.getHistoricoSemestral();
    }

    //INTERACTOR CALLBACK METHODS
    @Override
    public void onSuccess(HistoricoSemestralEntity historicoSemestralEntity) {
        view.setTableViewData(historicoSemestralEntity.getSemestres());
    }

    @Override
    public void onFailure(String error) {
        this.view.showAlertDialog("", error);
    }

}
