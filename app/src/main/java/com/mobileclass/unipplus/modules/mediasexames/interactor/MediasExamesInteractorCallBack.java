package com.mobileclass.unipplus.modules.mediasexames.interactor;

import com.mobileclass.unipplus.modules.mediasexames.entities.MediasExamesEntity;

public interface MediasExamesInteractorCallBack {
    void onSuccess(MediasExamesEntity mediasExamesEntity);
    void onFailure(String error);
}
