package com.mobileclass.unipplus.modules.home.interactor;

import androidx.fragment.app.Fragment;

import com.mobileclass.unipplus.modules.historicopagamentos.view.HistoricoPagamentosInteractorFragment;
import com.mobileclass.unipplus.modules.historicosemestral.view.HistoricoSemestralInteractorFragment;
import com.mobileclass.unipplus.modules.mediasexames.view.MediasExamesFragment;
import com.mobileclass.unipplus.modules.notasfaltas.view.NotasFaltasFragment;
import com.mobileclass.unipplus.modules.onlineclasses.view.OnlineClassesFragment;

import java.util.ArrayList;
import java.util.List;

public class ContentInteractor {
    private ContentInteractorCallBack callBack;

    public void setCallBack(ContentInteractorCallBack callBack) {
        this.callBack = callBack;
    }

    public void loadContents() {
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(new NotasFaltasFragment());
        fragments.add(new MediasExamesFragment());
        fragments.add(new HistoricoPagamentosInteractorFragment());
        fragments.add(new HistoricoSemestralInteractorFragment());
        callBack.onContentsLoaded(fragments);
    }

    public void loadContentsTitles() {
        String[] titles = {"Notas e Faltas", "Médias e Exames", "Pagamentos", "Histórico"};
        callBack.onContentsTitlesLoaded(titles);
    }
}
