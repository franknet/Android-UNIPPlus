package com.mobileclass.unipplus.modules.historicosemestral.entities;

import java.util.ArrayList;

public class HistoricoSemestralEntity {
    private ArrayList<HistoricoSemestralSemestreEntity> semestres;
    private String erro;

    public ArrayList<HistoricoSemestralSemestreEntity> getSemestres() {
        return semestres;
    }

    public void setSemestres(ArrayList<HistoricoSemestralSemestreEntity> semestres) {
        this.semestres = semestres;
    }

    public String getErro() {
        return erro;
    }
}
