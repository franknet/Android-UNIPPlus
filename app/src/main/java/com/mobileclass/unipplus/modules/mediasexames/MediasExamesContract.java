package com.mobileclass.unipplus.modules.mediasexames;

import com.mobileclass.unipplus.bases.BasePresenter;
import com.mobileclass.unipplus.bases.BaseView;
import com.mobileclass.unipplus.modules.mediasexames.entities.MediasExamesDisciplinaEntity;

import java.util.List;

public interface MediasExamesContract {
    interface View extends BaseView {
        void setTableViewData(List<List<MediasExamesDisciplinaEntity>> groupedMediasExamesList);
    }

    interface Presenter extends BasePresenter {

    }
}
