package com.mobileclass.unipplus.modules.notasfaltas.interactor;

import com.mobileclass.unipplus.modules.notasfaltas.entities.NotasFaltasEntity;

public interface NotasFaltasInteractorCallBack {
    void onSuccess(NotasFaltasEntity notasFaltasEntity);
    void onFailure(String errorMessage);
}
