package com.mobileclass.unipplus.modules.notasfaltas.entities;

public class NotasFaltasDisciplinaEntity {
    private String disciplina;
    private String faltas;
    private String nomedisciplina;
    private String np1;
    private String np2;
    private String obsdisciplina;
    private String tipodisciplina;
    private String turmaespecial;
    private String status;
    private int np1Color;
    private int np2Color;

    public String getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }

    public String getFaltas() {
        return faltas;
    }

    public void setFaltas(String faltas) {
        this.faltas = faltas;
    }

    public String getNomedisciplina() {
        return nomedisciplina;
    }

    public void setNomedisciplina(String nomedisciplina) {
        this.nomedisciplina = nomedisciplina;
    }

    public String getNp1() {
        return np1;
    }

    public void setNp1(String np1) {
        this.np1 = np1;
    }

    public String getNp2() {
        return np2;
    }

    public void setNp2(String np2) {
        this.np2 = np2;
    }

    public String getObsdisciplina() {
        return obsdisciplina;
    }

    public void setObsdisciplina(String obsdisciplina) {
        this.obsdisciplina = obsdisciplina;
    }

    public String getTipodisciplina() {
        return tipodisciplina;
    }

    public void setTipodisciplina(String tipodisciplina) {
        this.tipodisciplina = tipodisciplina;
    }

    public String getTurmaespecial() {
        return turmaespecial;
    }

    public void setTurmaespecial(String turmaespecial) {
        this.turmaespecial = turmaespecial;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getNp1Color() {
        return np1Color;
    }

    public void setNp1Color(int np1Color) {
        this.np1Color = np1Color;
    }

    public int getNp2Color() {
        return np2Color;
    }

    public void setNp2Color(int np2Color) {
        this.np2Color = np2Color;
    }
}
