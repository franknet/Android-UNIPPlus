package com.mobileclass.unipplus.modules.onlineclasses.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.mobileclass.unipplus.R;
import com.mobileclass.unipplus.bases.BaseFragment;
import com.mobileclass.unipplus.modules.onlineclasses.entities.OnlineClassEntity;
import com.mobileclass.unipplus.modules.onlineclasses.OnlineClassesContract;
import com.mobileclass.unipplus.modules.onlineclasses.OnlineClassesPresenter;
import com.mobileclass.unipplus.modules.onlineclasses.entities.OnlineClassZoomEntity;

import java.util.List;

public class OnlineClassesFragment extends BaseFragment implements OnlineClassesContract.View, OnlineClassesBaseAdapterOnItemSelected {

    private TextView txvErrorMessage;
    private RecyclerView recyclerView;
    private ViewFlipper viewFlipper;
    private OnlineClassesPresenter presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);
        this.presenter = new OnlineClassesPresenter(this);
        this.viewFlipper = (ViewFlipper) inflater.inflate(R.layout.fragment_online_classes, container, false);
        this.recyclerView = viewFlipper.findViewById(R.id.recyclerView);
        this.txvErrorMessage = viewFlipper.findViewById(R.id.txvErrorMessage);

        Button btnTryAgain = viewFlipper.findViewById(R.id.btnTryAgain);
        btnTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.start();
            }
        });

        return this.viewFlipper;
    }

    //VIEW METHODS
    @Override
    public void onResume() {
        super.onResume();
        this.presenter.start();
    }

    @Override
    public void showLoading(boolean show) {
        this.viewFlipper.setDisplayedChild(0);
    }

    @Override
    public void hideKeyboard() {
    }

    @Override
    public void showAlertDialog(String title, String message) {
        this.txvErrorMessage.setText(message);
        this.viewFlipper.setDisplayedChild(2);
    }

    @Override
    public void setTableViewData(List<OnlineClassEntity> classes) {
        this.viewFlipper.setDisplayedChild(1);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        this.recyclerView.setLayoutManager(layoutManager);
        OnlineClassesBaseAdapter adapter = new OnlineClassesBaseAdapter(getContext(), classes);
        adapter.setListener(this);
        this.recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean isRunningInBackground() {
        return super.isRunningInBackground();
    }

    @Override
    public void openZoomMeeting(OnlineClassZoomEntity zoom) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(zoom.getLinkAula()));
        if (intent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Toast.makeText(getContext(), "Você precisa ter o app Zoom intalado no seu device", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSelect(OnlineClassEntity item) {
        presenter.sendLog(item.getZoom());
    }
}