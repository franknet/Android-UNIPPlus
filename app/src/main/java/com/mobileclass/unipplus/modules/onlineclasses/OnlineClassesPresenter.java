package com.mobileclass.unipplus.modules.onlineclasses;

import com.mobileclass.unipplus.modules.onlineclasses.entities.OnlineClassZoomEntity;
import com.mobileclass.unipplus.modules.onlineclasses.interactor.OnlineClassesInteractor;
import com.mobileclass.unipplus.modules.onlineclasses.interactor.OnlineClassesInteractorCallBack;
import com.mobileclass.unipplus.modules.onlineclasses.entities.OnlineClassEntity;

import java.util.List;

public class OnlineClassesPresenter implements OnlineClassesContract.Presenter, OnlineClassesInteractorCallBack {
    private final OnlineClassesContract.View view;
    private final OnlineClassesInteractor interactor;


    public OnlineClassesPresenter(OnlineClassesContract.View view) {
        this.view = view;

        this.interactor = new OnlineClassesInteractor();
        this.interactor.setCallBack(this);
    }

    @Override
    public void start() {
        this.view.showLoading(true);
        this.interactor.getOnlineClasses();
    }

    @Override
    public void sendLog(OnlineClassZoomEntity zoom) {
        if (zoom != null) {
            interactor.postZoomLog(zoom);
        }
    }

    @Override
    public void onSuccess(List<OnlineClassEntity> classes) {
        this.view.setTableViewData(classes);
    }

    @Override
    public void onFailure(String errorMessage) {
        this.view.showAlertDialog("", errorMessage);
    }

    @Override
    public void onLogClassSuccess(OnlineClassZoomEntity zoom) {
        view.openZoomMeeting(zoom);
    }
}
