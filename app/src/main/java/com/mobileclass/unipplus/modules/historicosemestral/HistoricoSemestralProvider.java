package com.mobileclass.unipplus.modules.historicosemestral;

import com.mobileclass.unipplus.net.LinksUNIP;
import com.mobileclass.unipplus.net.http.HttpClient;
import com.mobileclass.unipplus.net.http.HttpProvider;

import okhttp3.RequestBody;

public class HistoricoSemestralProvider extends HttpProvider {
    @Override
    public String path() {
        return LinksUNIP.URL_HISTORICO_SEMESTRAL;
    }

    @Override
    public HttpClient.RequestMethod method() {
        return HttpClient.RequestMethod.Get;
    }

    @Override
    public RequestBody body() {
        return null;
    }
}
