package com.mobileclass.unipplus.modules.mediasexames.entities;

import java.util.ArrayList;

public class MediasExamesListEntity {
    private ArrayList<MediasExamesDisciplinaEntity> disciplinas;

    public ArrayList<MediasExamesDisciplinaEntity> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(ArrayList<MediasExamesDisciplinaEntity> disciplinas) {
        this.disciplinas = disciplinas;
    }

}
