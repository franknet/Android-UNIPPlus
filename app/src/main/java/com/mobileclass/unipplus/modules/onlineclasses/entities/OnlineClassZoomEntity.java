package com.mobileclass.unipplus.modules.onlineclasses.entities;

public class OnlineClassZoomEntity {
    private String CodDIsc;
    private String CodCurso;
    private String Turma;
    private String LinkAula;
    private String NomeDisc;
    private String HoraEntrada;
    private String MatriculaLog;
    private String DataInicio;
    private String DataFim;

    public String getCodDIsc() {
        return CodDIsc;
    }

    public void setCodDIsc(String codDIsc) {
        CodDIsc = codDIsc;
    }

    public String getCodCurso() {
        return CodCurso;
    }

    public void setCodCurso(String codCurso) {
        CodCurso = codCurso;
    }

    public String getTurma() {
        return Turma;
    }

    public void setTurma(String turma) {
        Turma = turma;
    }

    public String getLinkAula() {
        return LinkAula;
    }

    public void setLinkAula(String linkAula) {
        LinkAula = linkAula;
    }

    public String getNomeDisc() {
        return NomeDisc;
    }

    public void setNomeDisc(String nomeDisc) {
        NomeDisc = nomeDisc;
    }

    public String getHoraEntrada() {
        return HoraEntrada;
    }

    public void setHoraEntrada(String horaEntrada) {
        HoraEntrada = horaEntrada;
    }

    public String getMatriculaLog() {
        return MatriculaLog;
    }

    public void setMatriculaLog(String matriculaLog) {
        MatriculaLog = matriculaLog;
    }

    public String getDataInicio() {
        return DataInicio;
    }

    public void setDataInicio(String dataInicio) {
        DataInicio = dataInicio;
    }

    public String getDataFim() {
        return DataFim;
    }

    public void setDataFim(String dataFim) {
        DataFim = dataFim;
    }
}
