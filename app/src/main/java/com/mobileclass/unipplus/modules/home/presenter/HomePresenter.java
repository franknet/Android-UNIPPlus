package com.mobileclass.unipplus.modules.home.presenter;

import com.mobileclass.unipplus.modules.home.interactor.HomeInteractor;
import com.mobileclass.unipplus.modules.home.interactor.HomeInteractorCallBack;
import com.mobileclass.unipplus.modules.home.view.activities.HomeView;

public class HomePresenter implements HomeInteractorCallBack {
    private HomeView view;
    private HomeInteractor interactor;

    public HomePresenter(HomeView view) {
        this.view = view;
        this.view.setSideMenuView();
        this.view.setContentView();

        this.interactor = new HomeInteractor();
        this.interactor.setCallBack(this);
        this.interactor.checkRate();
    }

    public void onHomeClicked() {
        if (this.view.isSideMenuOpen()) {
            this.view.closeSideMenu();
        } else {
            this.view.openSideMenu();
        }
    }

    public void onBackButtonClicked() {
        this.view.showExitDialog();
    }

    public void onJaAvalieiClicked() {
        this.interactor.stopValidation();
    }

    //INTERACTOR CALLBACK METHODS
    @Override
    public void mustRate(String message) {
        this.view.showRateDialog(message);
    }
}
