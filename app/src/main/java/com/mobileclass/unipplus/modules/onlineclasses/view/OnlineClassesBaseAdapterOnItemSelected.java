package com.mobileclass.unipplus.modules.onlineclasses.view;

import com.mobileclass.unipplus.modules.onlineclasses.entities.OnlineClassEntity;

public interface OnlineClassesBaseAdapterOnItemSelected {
    void onSelect(OnlineClassEntity item);
}
