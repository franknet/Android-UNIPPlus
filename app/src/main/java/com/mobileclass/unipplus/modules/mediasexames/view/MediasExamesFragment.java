package com.mobileclass.unipplus.modules.mediasexames.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.mobileclass.unipplus.R;
import com.mobileclass.unipplus.bases.BaseFragment;
import com.mobileclass.unipplus.modules.mediasexames.MediasExamesContract;
import com.mobileclass.unipplus.modules.mediasexames.entities.MediasExamesDisciplinaEntity;
import com.mobileclass.unipplus.modules.mediasexames.MediasExamesPresenter;

import java.util.List;

public class MediasExamesFragment extends BaseFragment implements MediasExamesContract.View {
    private ViewFlipper viewFlipper;
    private TextView txvErrorMessage;
    private ExpandableListView elvMediasExames;
    private MediasExamesPresenter presenter;

    //LIFE CIRCLE METHODS
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);
        presenter = new MediasExamesPresenter(this);

        viewFlipper = (ViewFlipper) inflater.inflate(R.layout.fragment_medias_exames, container, false);
        elvMediasExames = viewFlipper.findViewById(R.id.elvMediasExames);
        txvErrorMessage = viewFlipper.findViewById(R.id.txvErrorMessage);

        Button btnTryAgain = viewFlipper.findViewById(R.id.btnTryAgain);
        btnTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.start();
            }
        });

        return viewFlipper;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    //VIEW METHODS
    @Override
    public void showLoading(boolean show) {
        viewFlipper.setDisplayedChild(0);
    }

    @Override
    public void hideKeyboard() {

    }

    @Override
    public void showAlertDialog(String title, String message) {
        viewFlipper.setDisplayedChild(2);
        txvErrorMessage.setText(message);
    }

    @Override
    public void setTableViewData(List<List<MediasExamesDisciplinaEntity>> groupedMediasExamesList) {
        viewFlipper.setDisplayedChild(1);
        MediasExamesBaseExpandableAdapter adapter = new MediasExamesBaseExpandableAdapter(groupedMediasExamesList, getContext());
        elvMediasExames.setAdapter(adapter);
        expandList(adapter);
    }

    private void expandList(ExpandableListAdapter adapter) {
        int index = 0;
        while (index < adapter.getGroupCount()) {
            elvMediasExames.expandGroup(index);
            index++;
        }
    }
}
