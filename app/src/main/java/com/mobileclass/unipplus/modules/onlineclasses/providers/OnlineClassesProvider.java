package com.mobileclass.unipplus.modules.onlineclasses.providers;

import com.mobileclass.unipplus.net.LinksUNIP;
import com.mobileclass.unipplus.net.http.HttpClient;
import com.mobileclass.unipplus.net.http.HttpProvider;

import okhttp3.RequestBody;

public class OnlineClassesProvider extends HttpProvider {
    private final String ra;

    public OnlineClassesProvider(String ra) {
        this.ra = ra;
    }

    @Override
    public String path() {
        return LinksUNIP.URL_ONLINE_CLASSES + this.ra;
    }

    @Override
    public HttpClient.RequestMethod method() {
        return null;
    }

    @Override
    public RequestBody body() {
        return null;
    }
}
