package com.mobileclass.unipplus.modules.home.presenter;

import androidx.fragment.app.Fragment;

import com.mobileclass.unipplus.modules.home.interactor.ContentInteractorCallBack;
import com.mobileclass.unipplus.modules.home.interactor.ContentInteractor;
import com.mobileclass.unipplus.modules.home.view.fragments.ContentView;

import java.util.List;

public class ContentPresenter implements ContentInteractorCallBack {
    private ContentView view;
    private ContentInteractor interactor;
    private String[] contentsTitles;

    public ContentPresenter(ContentView view) {
        this.view = view;
        view.setToolbar();

        this.interactor = new ContentInteractor();
        this.interactor.setCallBack(this);
        this.interactor.loadContentsTitles();
        this.interactor.loadContents();
    }

    public void contentSelected(int index) {
        this.view.setCurrentPageTitle(this.contentsTitles[index]);
    }

    //INTERACTOR CALLBACK METHODS
    @Override
    public void onContentsLoaded(List<Fragment> contents) {
        this.view.setContentsList(contents);
    }

    @Override
    public void onContentsTitlesLoaded(String[] titles) {
        this.contentsTitles = titles;
    }
}
