package com.mobileclass.unipplus.modules.historicopagamentos.interactor;

import com.mobileclass.unipplus.R;
import com.mobileclass.unipplus.modules.historicopagamentos.HistoricoPagamentosProvider;
import com.mobileclass.unipplus.modules.historicopagamentos.entities.HistoricoPagamentosEntity;
import com.mobileclass.unipplus.modules.historicopagamentos.entities.HistoricoPagamentosListEntity;
import com.mobileclass.unipplus.modules.historicopagamentos.entities.HistoricoPagamentosPosicaoEntity;
import com.mobileclass.unipplus.net.LinksUNIP;
import com.mobileclass.unipplus.net.http.HttpClient;
import com.mobileclass.unipplus.utils.NumberUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class HistoricoPagamentosInteractor {
    private static final String INTERNAL_ERROR = "Ocorreu um erro iniesperado, por favor tente novamente.";
    private HistoricoPagamentosInteractorCallBack callBack;

    public void setCallBack(HistoricoPagamentosInteractorCallBack callBack) {
        this.callBack = callBack;
    }

    public void getHistoricoPagamentos() {
        HistoricoPagamentosProvider provider = new HistoricoPagamentosProvider();
        HttpClient client = new HttpClient();
        client.doRequest(provider);
        client.result(new HttpClient.Result() {
            @Override
            public void onSuccess(int statusCode, byte[] data) {
                if (statusCode == 200) {
                    createHistoricoPagamentosEntity(data);
                } else {
                    callBack.onFailure("Sessão expirada, logue-se novamente");
                }
            }

            @Override
            public void onFailure(String message) {
                callBack.onFailure(message);
            }
        });
        client.execute();
    }

    private void createHistoricoPagamentosEntity(byte[] data) {
        HistoricoPagamentosListEntity historicoPagamentosListEntity = HistoricoPagamentosParser.parse(data);
        validateHistoricoPagamentosEntity(historicoPagamentosListEntity);
    }

    private void validateHistoricoPagamentosEntity(HistoricoPagamentosListEntity historicoPagamentosListEntity) {
        if (historicoPagamentosListEntity != null) {
            if (historicoPagamentosListEntity.getPosicao().isEmpty()) {
                callBack.onFailure("Relação de disciplinas não encontrada.");
            } else {
                List<List<HistoricoPagamentosPosicaoEntity>> groupedHistoricoPagamentosEntities = groupPosicaoEntityByType(historicoPagamentosListEntity.getPosicao());
                HistoricoPagamentosEntity historicoPagamentosEntity = new HistoricoPagamentosEntity();
                historicoPagamentosEntity.setGroupedHistoricoPagamentos(groupedHistoricoPagamentosEntities);
                callBack.onSuccess(historicoPagamentosEntity);
                String totalBills = "R$ " + getTotalBills(historicoPagamentosListEntity.getPosicao());
                callBack.totalBills(totalBills);
            }
        } else {
            callBack.onFailure(INTERNAL_ERROR);
        }
    }

    private List<List<HistoricoPagamentosPosicaoEntity>> groupPosicaoEntityByType(List<HistoricoPagamentosPosicaoEntity> posicaoEntities) {
        List<List<HistoricoPagamentosPosicaoEntity>> groupedPosicao = new ArrayList<>();
        for (int i = 0; i < posicaoEntities.size(); i++) {
            HistoricoPagamentosPosicaoEntity historicoPagamentosPosicaoEntity = posicaoEntities.get(i);
            validateSituacao(historicoPagamentosPosicaoEntity);
            if (i == 0) {
                groupedPosicao.add(filterPosicaoByCode(posicaoEntities, historicoPagamentosPosicaoEntity.getTipoparcela()));
            } else {
                HistoricoPagamentosPosicaoEntity previousHistoricoPagamentosPosicaoEntity = posicaoEntities.get(i - 1);
                if (!historicoPagamentosPosicaoEntity.getTipoparcela().equalsIgnoreCase(previousHistoricoPagamentosPosicaoEntity.getTipoparcela())) {
                    groupedPosicao.add(filterPosicaoByCode(posicaoEntities, historicoPagamentosPosicaoEntity.getTipoparcela()));
                }
            }
        }
        return groupedPosicao;
    }

    private List<HistoricoPagamentosPosicaoEntity> filterPosicaoByCode(List<HistoricoPagamentosPosicaoEntity> posicaoEntities, String code) {
        List<HistoricoPagamentosPosicaoEntity> filtredDisciplinas = new ArrayList<>();
        for (HistoricoPagamentosPosicaoEntity historicoPagamentosPosicaoEntity : posicaoEntities) {
            if (historicoPagamentosPosicaoEntity.getTipoparcela().equalsIgnoreCase(code)) {
                filtredDisciplinas.add(historicoPagamentosPosicaoEntity);
            }
        }
        return filtredDisciplinas;
    }

    private void validateSituacao(HistoricoPagamentosPosicaoEntity historicoPagamentosPosicaoEntity) {
        if (historicoPagamentosPosicaoEntity.getSituacao().trim().equalsIgnoreCase("bi")) {
            historicoPagamentosPosicaoEntity.setDatapagamento(historicoPagamentosPosicaoEntity.getDatavencimento());
        }
        if (historicoPagamentosPosicaoEntity.getSituacao().trim().equalsIgnoreCase("*deve")) {
            historicoPagamentosPosicaoEntity.setSituacaoColor(R.color.color_011);
            historicoPagamentosPosicaoEntity.setDatapagamento("");
        } else {
            historicoPagamentosPosicaoEntity.setSituacaoColor(R.color.color_008);
        }
    }

    private String getTotalBills(List<HistoricoPagamentosPosicaoEntity> posicaoEntities) {
        Double totalInvestimento = 0.00;
        DecimalFormat decFormat = new DecimalFormat("#,##0.00");
        for (HistoricoPagamentosPosicaoEntity historicoPagamentosPosicaoEntity : posicaoEntities) {
            if (historicoPagamentosPosicaoEntity.getSituacao().trim().equalsIgnoreCase("OK") || historicoPagamentosPosicaoEntity.getSituacao().trim().equalsIgnoreCase("BI")) {
                Double vl_pago = NumberUtils.decimalStringToDouble(historicoPagamentosPosicaoEntity.getValorpago());
                totalInvestimento = totalInvestimento + vl_pago;
            }
        }
        return decFormat.format(totalInvestimento);
    }
}
