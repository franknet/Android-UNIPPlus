package com.mobileclass.unipplus.modules.login.providers;

import com.mobileclass.unipplus.net.LinksUNIP;
import com.mobileclass.unipplus.net.http.HttpClient;
import com.mobileclass.unipplus.net.http.HttpProvider;

import okhttp3.RequestBody;

public class LoginSessionProvider extends HttpProvider {
    private final String mToken;

    public LoginSessionProvider(String token) {
        mToken = token;
    }

    @Override
    public String path() {
        return LinksUNIP.URL_HOME + mToken;
    }

    @Override
    public HttpClient.RequestMethod method() {
        return HttpClient.RequestMethod.Get;
    }

    @Override
    public RequestBody body() {
        return null;
    }
}
