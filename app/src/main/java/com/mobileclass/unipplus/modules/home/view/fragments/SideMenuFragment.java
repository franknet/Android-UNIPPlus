package com.mobileclass.unipplus.modules.home.view.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.mobileclass.unipplus.customviews.CircularImageView;
import com.mobileclass.unipplus.R;
import com.mobileclass.unipplus.modules.home.entities.SideMenuListEntity;
import com.mobileclass.unipplus.modules.home.presenter.SideMenuPresenter;
import com.mobileclass.unipplus.modules.home.view.adapters.SideMenuBaseExpandableAdapter;

import java.util.ArrayList;

public class SideMenuFragment extends Fragment implements SideMenuView, OnClickListener, ExpandableListView.OnGroupClickListener {
    private CircularImageView imageViewUserPhoto;
    private TextView textViewUsername, textViewCourseName;
    private ExpandableListView elvSideMenu;
    private SideMenuPresenter presenter;

    public SideMenuFragment() {
        // Required empty public constructor
    }

    //FRAGMENT LIFE CIRCLE
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_side_menu, container, false);

        this.imageViewUserPhoto         = view.findViewById(R.id.imageViewUserPhoto);
        this.textViewUsername           = view.findViewById(R.id.textViewUserName);
        this.textViewCourseName         = view.findViewById(R.id.textViewCourseName);

        Button buttonExit = (Button) view.findViewById(R.id.buttonExit);
        buttonExit.setOnClickListener(this);

        this.elvSideMenu = view.findViewById(R.id.elvSideMenu);
        this.elvSideMenu.setOnGroupClickListener(this);

        this.presenter = new SideMenuPresenter(this);

        return view;
    }

    //PRESENTER METHODS
    @Override
    public void setMenuContents(ArrayList<SideMenuListEntity> sideMenuListEntities) {
        this.elvSideMenu.setAdapter(new SideMenuBaseExpandableAdapter(getContext(), sideMenuListEntities));
    }

    @Override
    public void setUserName(String name) {
        this.textViewUsername.setText(name);
    }

    @Override
    public void setCourseName(String course) {
        this.textViewCourseName.setText(course);
    }

    @Override
    public void setUserProfile(Bitmap profile) {
        imageViewUserPhoto.setVisibility(View.VISIBLE);
        this.imageViewUserPhoto.setImageBitmap(profile);
    }

    @Override
    public void goTo(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void startLibrayActivity() {

    }

    //EVENTS
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.buttonExit) {
            if (getActivity() != null) {
                getActivity().onBackPressed();
            }
        }

    }

    @Override
    public boolean onGroupClick(ExpandableListView expandableListView, View view, int groupPosition, long id) {

        if (this.presenter != null) {
            this.presenter.groupSelected((int) id);
        }

        return false;
    }
}
