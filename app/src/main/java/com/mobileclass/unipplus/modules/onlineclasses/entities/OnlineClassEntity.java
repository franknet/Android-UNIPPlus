package com.mobileclass.unipplus.modules.onlineclasses.entities;

public class OnlineClassEntity {

    public enum StatusCode {
        PENDING, ON_LIVE, HAS_PASSED
    }

    private String discipline;
    private String datetime;
    private String link;
    private String status;
    private StatusCode statusCode;
    private OnlineClassZoomEntity zoom;

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public StatusCode getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(StatusCode statusCode) {
        this.statusCode = statusCode;
    }

    public OnlineClassZoomEntity getZoom() {
        return zoom;
    }

    public void setZoom(OnlineClassZoomEntity zoom) {
        this.zoom = zoom;
    }
}
