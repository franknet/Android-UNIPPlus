package com.mobileclass.unipplus.modules.home.entities;

public class RateEntity {
    private int versionCode;
    private int count;
    private boolean validateCount;

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isValidateCount() {
        return validateCount;
    }

    public void setValidateCount(boolean validateCount) {
        this.validateCount = validateCount;
    }
}
