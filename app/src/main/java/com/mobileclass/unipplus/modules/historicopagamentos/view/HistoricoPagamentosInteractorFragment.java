package com.mobileclass.unipplus.modules.historicopagamentos.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.mobileclass.unipplus.R;
import com.mobileclass.unipplus.bases.BaseFragment;
import com.mobileclass.unipplus.modules.historicopagamentos.HIstoricoPagamentosContract;
import com.mobileclass.unipplus.modules.historicopagamentos.HistoricoPagamentosPresenter;
import com.mobileclass.unipplus.modules.historicopagamentos.entities.HistoricoPagamentosPosicaoEntity;

import java.util.List;

public class HistoricoPagamentosInteractorFragment extends BaseFragment implements HIstoricoPagamentosContract.View {
    private ExpandableListView elvHistoricoPagamentos;
    private TextView txvHistPgtoInvest;
    private TextView txvHistPgtoErrorMsg;
    private ViewFlipper viewFlipper;
    private HIstoricoPagamentosContract.Presenter presenter;

    //LIFE CIRCLE METHODS
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);
        this.presenter = new HistoricoPagamentosPresenter(this);
        this.viewFlipper = (ViewFlipper) inflater.inflate(R.layout.fragment_historico_pagamentos, container, false);
        this.elvHistoricoPagamentos = (ExpandableListView) viewFlipper.findViewById(R.id.elvHistoricoPagamentos);
        this.txvHistPgtoInvest = (TextView) viewFlipper.findViewById(R.id.txvHistPgtoInvest);
        this.txvHistPgtoErrorMsg = (TextView) viewFlipper.findViewById(R.id.txvErrorMessage);
        Button btnTryAgain = viewFlipper.findViewById(R.id.btnTryAgain);
        btnTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.start();
            }
        });
        return this.viewFlipper;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    //VIEW METHODS
    @Override
    public void showLoading(boolean show) {
        this.viewFlipper.setDisplayedChild(0);
    }

    @Override
    public void hideKeyboard() {

    }

    @Override
    public void showAlertDialog(String title, String message) {
        this.viewFlipper.setDisplayedChild(2);
        this.txvHistPgtoErrorMsg.setText(message);
    }

    @Override
    public void setTotalBills(String total) {
        this.txvHistPgtoInvest.setText(total);
    }

    @Override
    public void setTableViewData(List<List<HistoricoPagamentosPosicaoEntity>> groupedHistoricoPagamentos) {
        this.viewFlipper.setDisplayedChild(1);
        HistoricoPagamentosBaseExpandableAdapter adapter = new HistoricoPagamentosBaseExpandableAdapter(groupedHistoricoPagamentos, getContext());
        elvHistoricoPagamentos.setAdapter(adapter);
        expandListView(adapter);
    }

    private void expandListView(BaseExpandableListAdapter adapter) {
        int index = 0;
        while (index < adapter.getGroupCount()) {
            elvHistoricoPagamentos.expandGroup(index);
            index++;
        }
    }
}
