package com.mobileclass.unipplus.modules.onlineclasses.interactor;

import com.mobileclass.unipplus.UNIPPlusApplication;
import com.mobileclass.unipplus.modules.login.entity.UserEntity;
import com.mobileclass.unipplus.modules.onlineclasses.entities.OnlineClassEntity;
import com.mobileclass.unipplus.modules.onlineclasses.entities.OnlineClassZoomEntity;
import com.mobileclass.unipplus.modules.onlineclasses.providers.OnlineClassesLogProvider;
import com.mobileclass.unipplus.modules.onlineclasses.providers.OnlineClassesProvider;
import com.mobileclass.unipplus.net.http.HttpClient;

import java.util.List;

public class OnlineClassesInteractor {
    private OnlineClassesInteractorCallBack callBack;

    public void setCallBack(OnlineClassesInteractorCallBack callBack) {
        this.callBack = callBack;
    }

    public void getOnlineClasses() {
        UserEntity user = UNIPPlusApplication.getInstance().getCache().getUserEntity();
        OnlineClassesProvider provider = new OnlineClassesProvider(user.getRa().toUpperCase());

        HttpClient client = new HttpClient();
        client.doRequest(provider);
        client.result(new HttpClient.Result() {
            @Override
            public void onSuccess(int statusCode, byte[] data) {
                List<OnlineClassEntity> classes = OnlineClassesParser.create(data);
                callBack.onSuccess(classes);
            }

            @Override
            public void onFailure(String message) {
                callBack.onFailure(message);
            }
        });
        client.execute();
    }

    public void postZoomLog(final OnlineClassZoomEntity zoom) {
        OnlineClassesLogProvider provider = new OnlineClassesLogProvider(zoom);
        HttpClient client = new HttpClient();
        client.doRequest(provider);
        client.result(new HttpClient.Result() {
            @Override
            public void onSuccess(int statusCode, byte[] data) {
                callBack.onLogClassSuccess(zoom);
            }

            @Override
            public void onFailure(String message) {
                callBack.onFailure(message);
            }
        });
        client.execute();
    }

}
