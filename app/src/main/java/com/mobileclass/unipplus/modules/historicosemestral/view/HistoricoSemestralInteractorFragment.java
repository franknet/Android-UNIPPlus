package com.mobileclass.unipplus.modules.historicosemestral.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.mobileclass.unipplus.R;
import com.mobileclass.unipplus.bases.BaseFragment;
import com.mobileclass.unipplus.modules.historicosemestral.HistoricoSemestralContract;
import com.mobileclass.unipplus.modules.historicosemestral.HistoricoSemestralPresenter;
import com.mobileclass.unipplus.modules.historicosemestral.entities.HistoricoSemestralSemestreEntity;
import com.mobileclass.unipplus.utils.SystemUtils;

import java.util.List;

public class HistoricoSemestralInteractorFragment extends BaseFragment implements HistoricoSemestralContract.View {
    private ExpandableListView elvHistoricoSemestral;
    private TextView txvHistoricoSemestralErrorMsg;
    private ViewFlipper viewFlipper;
    private HistoricoSemestralPresenter presenter;

    //LIFE CIRCLE METHODS
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);
        this.presenter = new HistoricoSemestralPresenter(this);
        this.viewFlipper = (ViewFlipper) inflater.inflate(R.layout.fragment_historico_semestral, container, false);
        this.elvHistoricoSemestral = (ExpandableListView) viewFlipper.findViewById(R.id.elvHistoricoSemestral);
        this.txvHistoricoSemestralErrorMsg = (TextView) viewFlipper.findViewById(R.id.txvErrorMessage);
        Button btnTryAgain = viewFlipper.findViewById(R.id.btnTryAgain);
        btnTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.start();
            }
        });
        return this.viewFlipper;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    //VIEW METHODS
    @Override
    public void showLoading(boolean show) {
        this.viewFlipper.setDisplayedChild(0);
    }

    @Override
    public void hideKeyboard() {

    }

    @Override
    public void showAlertDialog(String title, String message) {
        this.viewFlipper.setDisplayedChild(2);
        txvHistoricoSemestralErrorMsg.setText(message);
    }

    @Override
    public void setTableViewData(List<HistoricoSemestralSemestreEntity> historicoSemestralSemestreEntities) {
        this.viewFlipper.setDisplayedChild(1);
        HistoricoSemestralBaseExpandableAdapter adapter = new HistoricoSemestralBaseExpandableAdapter(historicoSemestralSemestreEntities, getContext());
        this.elvHistoricoSemestral.setAdapter(adapter);
        expandListView(adapter);
    }

    private void expandListView(BaseExpandableListAdapter adapter) {
        int index = 0;
        while (index < adapter.getGroupCount()) {
            elvHistoricoSemestral.expandGroup(index);
            index++;
        }
    }
}
