package com.mobileclass.unipplus.modules.mediasexames;

import com.mobileclass.unipplus.modules.mediasexames.entities.MediasExamesEntity;
import com.mobileclass.unipplus.modules.mediasexames.interactor.MediasExamesInteractor;
import com.mobileclass.unipplus.modules.mediasexames.interactor.MediasExamesInteractorCallBack;

public class MediasExamesPresenter implements MediasExamesContract.Presenter, MediasExamesInteractorCallBack {
    private MediasExamesContract.View view;
    private MediasExamesInteractor interactor;

    public MediasExamesPresenter(MediasExamesContract.View view) {
        this.view = view;
        interactor = new MediasExamesInteractor();
        interactor.setCallBack(this);
    }

    @Override
    public void start() {
        view.showLoading(true);
        interactor.getMediasExames();
    }

    //INTERACTOR CALLBACK METHODS
    @Override
    public void onSuccess(MediasExamesEntity mediasExamesEntity) {
        view.setTableViewData(mediasExamesEntity.getGroupedMediasExamesList());
    }

    @Override
    public void onFailure(String error) {
        view.showAlertDialog("", error);
    }
}
