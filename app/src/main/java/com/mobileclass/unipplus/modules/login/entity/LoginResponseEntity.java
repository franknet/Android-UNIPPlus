package com.mobileclass.unipplus.modules.login.entity;

import java.io.Serializable;

public class LoginResponseEntity implements Serializable {
    public boolean valida = false;
    public String identificacao;
    public String nomeUsuario;
    public String cursoCod;
    public String curso;
    public String unidade;
    public String token;
    public String mensagemInvalida;
    public String ensino;
    public String email;
    public String situacao;
    public String instituto;
}
