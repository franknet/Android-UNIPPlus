package com.mobileclass.unipplus.modules.historicopagamentos.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdService;
import com.google.android.gms.ads.AdView;
import com.mobileclass.unipplus.R;
import com.mobileclass.unipplus.modules.historicopagamentos.entities.HistoricoPagamentosPosicaoEntity;
import com.mobileclass.unipplus.services.google.admob.AdMobService;
import com.mobileclass.unipplus.services.google.admob.BannerAdListener;

import java.util.List;

public class HistoricoPagamentosBaseExpandableAdapter extends BaseExpandableListAdapter {
    private LayoutInflater layoutInflater;
    private List<List<HistoricoPagamentosPosicaoEntity>> groupedHistoricoPagamentosEntities;

    public HistoricoPagamentosBaseExpandableAdapter(List<List<HistoricoPagamentosPosicaoEntity>> groupedHistoricoPagamentos, Context context) {
        this.groupedHistoricoPagamentosEntities = groupedHistoricoPagamentos;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getGroupCount() {
        return groupedHistoricoPagamentosEntities.size();
    }

    @Override
    public int getChildrenCount(int i) {
        List<HistoricoPagamentosPosicaoEntity> posicaoEntities = groupedHistoricoPagamentosEntities.get(i);
        return posicaoEntities.size();
    }

    @Override
    public Object getGroup(int i) {
        return null;
    }

    @Override
    public Object getChild(int i, int i1) {
        return null;
    }

    @Override
    public long getGroupId(int i) {
        return 0;
    }

    @Override
    public long getChildId(int i, int i1) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = this.layoutInflater.inflate(R.layout.list_group_item_default, parent, false);

            viewHolder.txvGroupTitle = (TextView) convertView.findViewById(R.id.txvGroupTitle);
            viewHolder.txvGroupCount = (TextView) convertView.findViewById(R.id.txvGroupCount);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        List<HistoricoPagamentosPosicaoEntity> posicaoEntities = groupedHistoricoPagamentosEntities.get(i);
        HistoricoPagamentosPosicaoEntity historicoPagamentosPosicaoEntity = posicaoEntities.get(0);

        viewHolder.txvGroupTitle.setText(historicoPagamentosPosicaoEntity.getDescricaodivida());
        viewHolder.txvGroupCount.setText(String.valueOf(posicaoEntities.size()));

        return convertView;
    }

    @Override
    public View getChildView(int groupIndex, int childIndex, boolean b, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        List<HistoricoPagamentosPosicaoEntity> posicaoEntities = groupedHistoricoPagamentosEntities.get(groupIndex);
        HistoricoPagamentosPosicaoEntity historicoPagamentosPosicaoEntity = posicaoEntities.get(childIndex);

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = this.layoutInflater.inflate(R.layout.list_child_item_historico_pagamentos, parent, false);

            viewHolder.txvHistPgtoTp = (TextView) convertView.findViewById(R.id.txvHistPgtoTp);
            viewHolder.txvHistPgtoDtVenc = (TextView) convertView.findViewById(R.id.txvHistPgtoDtVenc);
            viewHolder.txvHistPgtoVlDoc = (TextView) convertView.findViewById(R.id.txvHistPgtoVlDoc);
            viewHolder.txvHistPgtoObs = (TextView) convertView.findViewById(R.id.txvHistPgtoObs);
            viewHolder.txvHistPgtoDtPgto = (TextView) convertView.findViewById(R.id.txvHistPgtoDtPgto);
            viewHolder.txvHistPgtoVlPago = (TextView) convertView.findViewById(R.id.txvHistPgtoVlPago);
            viewHolder.advBanner = (AdView) convertView.findViewById(R.id.advBanner);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        int bannerIndex = groupedHistoricoPagamentosEntities.get(groupIndex).size() / 2;
        if (childIndex == bannerIndex) {
            AdMobService.requestBannerAd(viewHolder.advBanner, new BannerAdListener() {
                @Override
                public void onAdLoadFinished(AdView adView) {
                    adView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAdLoadFinishedWithError(AdView adView, String message) {
                }
            });

            AdRequest adRequest = new AdRequest.Builder().build();
            viewHolder.advBanner.loadAd(adRequest);
        } else {
            viewHolder.advBanner.setVisibility(View.GONE);
        }

        viewHolder.txvHistPgtoTp.setText(historicoPagamentosPosicaoEntity.getTipoparcela());
        viewHolder.txvHistPgtoDtVenc.setText(historicoPagamentosPosicaoEntity.getDatavencimento());
        viewHolder.txvHistPgtoVlDoc.setText(historicoPagamentosPosicaoEntity.getValor());
        viewHolder.txvHistPgtoObs.setText(historicoPagamentosPosicaoEntity.getSituacao().trim());
        viewHolder.txvHistPgtoDtPgto.setText(historicoPagamentosPosicaoEntity.getDatapagamento());
        viewHolder.txvHistPgtoVlPago.setText(historicoPagamentosPosicaoEntity.getValorpago());

        viewHolder.txvHistPgtoObs.setTextColor(convertView.getContext().getResources().getColor(historicoPagamentosPosicaoEntity.getSituacaoColor()));

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }

    private class ViewHolder {
        AdView advBanner;
        TextView txvHistPgtoTp;
        TextView txvHistPgtoDtVenc;
        TextView txvHistPgtoVlDoc;
        TextView txvHistPgtoObs;
        TextView txvHistPgtoDtPgto;
        TextView txvHistPgtoVlPago;
        TextView txvGroupTitle;
        TextView txvGroupCount;
    }
}
