package com.mobileclass.unipplus.modules.notasfaltas.view;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.mobileclass.unipplus.R;
import com.mobileclass.unipplus.customviews.CircularProgressbar;
import com.mobileclass.unipplus.modules.notasfaltas.entities.NotasFaltasDisciplinaEntity;
import com.mobileclass.unipplus.services.google.admob.AdMobService;
import com.mobileclass.unipplus.services.google.admob.BannerAdListener;
import com.mobileclass.unipplus.utils.NumberUtils;

import java.util.List;

public class NotasFaltasBaseExpandableAdapter extends BaseExpandableListAdapter {
    private final List<List<NotasFaltasDisciplinaEntity>> groupedDisciplinas;
    private final LayoutInflater layoutInflater;

    public NotasFaltasBaseExpandableAdapter(List<List<NotasFaltasDisciplinaEntity>> groupedDisciplinaEntities, Context context) {
        this.groupedDisciplinas = groupedDisciplinaEntities;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getGroupCount() {
        return this.groupedDisciplinas.size();
    }

    @Override
    public int getChildrenCount(int groupIndex) {
        List<NotasFaltasDisciplinaEntity> disciplinaEntities = groupedDisciplinas.get(groupIndex);
        return disciplinaEntities.size();
    }

    @Override
    public Object getGroup(int i) {
        return null;
    }

    @Override
    public Object getChild(int i, int i1) {
        return null;
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = this.layoutInflater.inflate(R.layout.list_group_item_default, parent, false);
            viewHolder.txvGroupTitle = convertView.findViewById(R.id.txvGroupTitle);
            viewHolder.txvGroupCount = convertView.findViewById(R.id.txvGroupCount);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        List<NotasFaltasDisciplinaEntity> disciplinaEntities = groupedDisciplinas.get(i);
        NotasFaltasDisciplinaEntity disciplinaEntity = disciplinaEntities.get(0);
        viewHolder.txvGroupTitle.setText(disciplinaEntity.getTipodisciplina());
        viewHolder.txvGroupCount.setText(String.valueOf(disciplinaEntities.size()));
        return convertView;
    }

    @Override
    public View getChildView(int groupIndex, int childIndex, boolean b, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        List<NotasFaltasDisciplinaEntity> disciplinaEntities = groupedDisciplinas.get(groupIndex);
        NotasFaltasDisciplinaEntity disciplinaEntity = disciplinaEntities.get(childIndex);
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = this.layoutInflater.inflate(R.layout.list_child_item_notas_faltas, parent, false);
            viewHolder.txvNotasFaltasDisciplina     = convertView.findViewById(R.id.txvNotasFaltasDisciplina);
            viewHolder.txvNotasFaltasNP1            = convertView.findViewById(R.id.txvNotasFaltasNP1);
            viewHolder.txvNotasFaltasNP2            = convertView.findViewById(R.id.txvNotasFaltasNP2);
            viewHolder.cpbNotasFaltasNP1            = convertView.findViewById(R.id.cpbNotasFaltasNP1);
            viewHolder.cpbNotasFaltasNP2            = convertView.findViewById(R.id.cpbNotasFaltasNP2);
            viewHolder.txvNotasFaltasFaltas         = convertView.findViewById(R.id.txvNotasFaltasFaltas);
            viewHolder.txvAdvise                    = convertView.findViewById(R.id.txvNP2Advise);
            viewHolder.advBanner                    = convertView.findViewById(R.id.advBanner);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        int bannerIndex = groupedDisciplinas.get(groupIndex).size() / 2;
        if (childIndex == bannerIndex) {
            AdMobService.requestBannerAd(viewHolder.advBanner, new BannerAdListener() {
                @Override
                public void onAdLoadFinished(AdView adView) {
                    adView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAdLoadFinishedWithError(AdView adView, String message) {
                }
            });
        } else {
            viewHolder.advBanner.setVisibility(View.GONE);
        }

        Double np1 = NumberUtils.decimalStringToDouble(disciplinaEntity.getNp1());
        Double np2 = NumberUtils.decimalStringToDouble(disciplinaEntity.getNp2());
        viewHolder.txvNotasFaltasDisciplina.setText(disciplinaEntity.getNomedisciplina());
        viewHolder.txvNotasFaltasNP1.setText(String.valueOf(np1));
        viewHolder.txvNotasFaltasNP2.setText(String.valueOf(np2));
        viewHolder.txvNotasFaltasFaltas.setText("Faltas: " + disciplinaEntity.getFaltas());
        viewHolder.txvAdvise.setText(Html.fromHtml(disciplinaEntity.getStatus()));
        int np1Color = convertView.getContext().getResources().getColor(disciplinaEntity.getNp1Color());
        viewHolder.txvNotasFaltasNP1.setTextColor(np1Color);
        viewHolder.cpbNotasFaltasNP1.setColor(np1Color);
        viewHolder.cpbNotasFaltasNP1.setProgress(NumberUtils.decimalStringToInt(disciplinaEntity.getNp1()));

        int np2Color = convertView.getContext().getResources().getColor(disciplinaEntity.getNp2Color());
        viewHolder.txvNotasFaltasNP2.setTextColor(np2Color);
        viewHolder.cpbNotasFaltasNP2.setColor(np2Color);
        viewHolder.cpbNotasFaltasNP2.setProgress(NumberUtils.decimalStringToInt(disciplinaEntity.getNp2()));

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }

    private class ViewHolder {
        AdView advBanner;
        TextView txvNotasFaltasDisciplina;
        TextView txvNotasFaltasNP1;
        TextView txvNotasFaltasNP2;
        TextView txvNotasFaltasFaltas;
        TextView txvGroupTitle;
        TextView txvGroupCount;
        TextView txvAdvise;
        CircularProgressbar cpbNotasFaltasNP1;
        CircularProgressbar cpbNotasFaltasNP2;
    }
}
