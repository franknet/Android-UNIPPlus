package com.mobileclass.unipplus.modules.home.view.fragments;

import androidx.fragment.app.Fragment;

import java.util.List;

/**
 * Created by hackintosh on 19/03/2017.
 */

public interface ContentView {
    void setToolbar();
    void setContentsList(List<Fragment> contents);
    void setCurrentPageTitle(String title);
}
