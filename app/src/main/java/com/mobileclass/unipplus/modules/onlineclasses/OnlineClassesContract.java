package com.mobileclass.unipplus.modules.onlineclasses;

import com.mobileclass.unipplus.bases.BasePresenter;
import com.mobileclass.unipplus.bases.BaseView;
import com.mobileclass.unipplus.modules.onlineclasses.entities.OnlineClassEntity;
import com.mobileclass.unipplus.modules.onlineclasses.entities.OnlineClassZoomEntity;

import java.util.List;

public interface OnlineClassesContract {
    interface View extends BaseView {
        void setTableViewData(List<OnlineClassEntity> classes);
        void openZoomMeeting(OnlineClassZoomEntity zoom);
    }

    interface Presenter extends BasePresenter {
        void sendLog(OnlineClassZoomEntity zoom);
    }
}
