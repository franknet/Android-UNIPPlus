package com.mobileclass.unipplus.modules.login;

import com.mobileclass.unipplus.bases.BasePresenter;
import com.mobileclass.unipplus.bases.BaseView;
import com.mobileclass.unipplus.modules.login.entity.LoginResponseEntity;
import com.mobileclass.unipplus.modules.login.entity.UserEntity;

public interface LoginContract {
    interface View extends BaseView {
        void setRA(String ra);
        void setPassword(String password);
        void checkKeepCredentials(boolean checked);
        void startHomeActivity(LoginResponseEntity result);
        void showNewVersionAlert();
    }

    interface Presenter extends BasePresenter {
        void onEntrarButtonClicked(UserEntity userEntity);
    }
}
