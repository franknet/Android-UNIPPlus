package com.mobileclass.unipplus.modules.notasfaltas;

import com.mobileclass.unipplus.net.LinksUNIP;
import com.mobileclass.unipplus.net.http.HttpClient;
import com.mobileclass.unipplus.net.http.HttpProvider;

import okhttp3.RequestBody;

public class NotasFaltasProvider extends HttpProvider {
    @Override
    public String path() {
        return LinksUNIP.URL_NOTAS_FALTAS;
    }

    @Override
    public HttpClient.RequestMethod method() {
        return HttpClient.RequestMethod.Get;
    }

    @Override
    public RequestBody body() {
        return null;
    }
}
