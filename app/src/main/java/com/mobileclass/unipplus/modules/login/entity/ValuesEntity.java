package com.mobileclass.unipplus.modules.login.entity;

/**
 * Created by Jose Franklin on 30/08/2017.
 */

public class ValuesEntity {
    private String text;
    private String password;

    public ValuesEntity(String csv) {
        String[] values = csv.split("\\|");
        this.text = values[0];
        this.password = values[1];
    }

    public String getText() {
        return text;
    }

    public String getPassword() {
        return password;
    }
}
