package com.mobileclass.unipplus.modules.historicosemestral;

import com.mobileclass.unipplus.bases.BasePresenter;
import com.mobileclass.unipplus.bases.BaseView;
import com.mobileclass.unipplus.modules.historicosemestral.entities.HistoricoSemestralSemestreEntity;

import java.util.List;

public interface HistoricoSemestralContract {
    interface View extends BaseView {
        void setTableViewData(List<HistoricoSemestralSemestreEntity> historicoSemestralSemestreEntities);
    }

    interface Presenter extends BasePresenter {

    }
}
