package com.mobileclass.unipplus.modules.login.entity;

public class LoginEntity {
    private String acessoliberado;
    private String chavelogado;
    private String erro;

    public boolean acessoliberado() {
        return Boolean.valueOf(acessoliberado);
    }

    public String getChavelogado() {
        return chavelogado;
    }

    public String getErro() {
        return erro;
    }
}
