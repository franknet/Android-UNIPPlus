package com.mobileclass.unipplus.modules.mediasexames.entities;

import java.util.List;

public class MediasExamesEntity {
    private List<List<MediasExamesDisciplinaEntity>> groupedMediasExamesList;

    public List<List<MediasExamesDisciplinaEntity>> getGroupedMediasExamesList() {
        return groupedMediasExamesList;
    }

    public void setGroupedMediasExamesList(List<List<MediasExamesDisciplinaEntity>> groupedMediasExamesList) {
        this.groupedMediasExamesList = groupedMediasExamesList;
    }
}
