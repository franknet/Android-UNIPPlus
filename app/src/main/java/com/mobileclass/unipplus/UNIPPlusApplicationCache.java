package com.mobileclass.unipplus;

import android.content.Context;

import com.mobileclass.unipplus.directory.SharedPrefDataAccess;
import com.mobileclass.unipplus.modules.home.entities.RateEntity;
import com.mobileclass.unipplus.modules.login.entity.UserEntity;
import com.mobileclass.unipplus.utils.JSONUtils;

public class UNIPPlusApplicationCache {
    private Context mContext;

    UNIPPlusApplicationCache(Context context) {
        this.mContext = context;
    }

    public UserEntity getUserEntity() {
        String strJsCredentials = new SharedPrefDataAccess(mContext).getData(SharedPrefDataAccess.DATA_USER);
        UserEntity userEntity = null;
        if (!strJsCredentials.isEmpty()) {
            userEntity = JSONUtils.deserialize(strJsCredentials, UserEntity.class);
        }
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        String strJsCredentials = JSONUtils.serialize(userEntity);
        new SharedPrefDataAccess(mContext).saveData(SharedPrefDataAccess.DATA_USER, strJsCredentials);
    }

    public RateEntity getRateEntity() {
        String rateJson = new SharedPrefDataAccess(mContext).getData(SharedPrefDataAccess.DATA_RATE);
        RateEntity rateEntity;
        if (rateJson.isEmpty()) {
            rateEntity = new RateEntity();
            rateEntity.setCount(0);
            rateEntity.setValidateCount(true);
            rateEntity.setVersionCode(0);
        } else {
            rateEntity = JSONUtils.deserialize(rateJson, RateEntity.class);
        }
        return rateEntity;
    }

    public void setRateEntity(RateEntity rateEntity) {
        String rateJson = JSONUtils.serialize(rateEntity);
        new SharedPrefDataAccess(mContext).saveData(SharedPrefDataAccess.DATA_RATE, rateJson);
    }
}
