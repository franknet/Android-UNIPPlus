package com.mobileclass.unipplus.utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class JSONUtils {
    public static String serialize(Object model) {
        String jsonString = null;

        if (model != null) {
            if (model instanceof Map) {
                JSONObject jsonObject = new JSONObject();

                Map<String, Object> map = (Map<String, Object>) model;
                Set<String> keys = map.keySet();

                for (String key: keys) {

                }
            }


            Field[] fields = model.getClass().getDeclaredFields();
            JSONObject jsonObject = new JSONObject();

            for (Field field : fields) {
                field.setAccessible(true);
                if (field.getType() == List.class || field.getType() == ArrayList.class ) {
                    try {
                        List<Object> list = (List<Object>) field.get(model);
                        JSONArray jsonArray = listToJSONArray(list);
                        jsonObject.put(field.getName(), jsonArray);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        jsonObject.put(field.getName(), field.get(model));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            jsonString = jsonObject.toString();

        }


        return jsonString;
    }

    public static <T> T deserialize(String json, Class jsonClass) {
        T object = null;

        try {
            JSONObject jsonBodyObject = new JSONObject(json);
            object = (T) jsonClass.newInstance();

            Field[] fields = object.getClass().getDeclaredFields();

            for (Field field : fields) {
                field.setAccessible(true);

                if (jsonBodyObject.has(field.getName()) && !jsonBodyObject.isNull(field.getName())) {
                    Object jsonObject = jsonBodyObject.get(field.getName());

                    if (field.getType() == List.class || field.getType() == ArrayList.class && jsonObject instanceof  JSONArray) {
                        Object listObject = jsonArrayToList(field, (JSONArray) jsonObject);
                        field.set(object, listObject);
                    } else {
                        if (jsonObject instanceof JSONObject) {
                            JSONObject customJsonObject = (JSONObject) jsonObject;
                            Object customObject = field.getType().newInstance();
                            Field[] customObjectFields = customObject.getClass().getDeclaredFields();

                            for (Field customObjectField : customObjectFields) {
                                customObjectField.setAccessible(true);
                                customObjectField.set(customObject, customJsonObject.get(customObjectField.getName()));
                            }

                            field.set(object, customObject);

                        } else {
                            field.set(object, jsonBodyObject.get(field.getName()));
                        }
                    }

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return object;
    }

    private static Object jsonArrayToList(Field list, JSONArray jsonArray) {
        List<Object> arrayList = null;

        try {
            ParameterizedType parameterizedType = (ParameterizedType) list.getGenericType();
            Class<?> listGenectClass = (Class<?>) parameterizedType.getActualTypeArguments()[0];
            arrayList = (ArrayList<Object>) list.getType().newInstance();

            Object listItem;

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                listItem = listGenectClass.newInstance();

                Field[] fields = listItem.getClass().getDeclaredFields();

                for (Field field : fields) {
                    field.setAccessible(true);

                    if (jsonObject.has(field.getName()) && !jsonObject.isNull(field.getName())) {
                        Object jsonObject1 = jsonObject.get(field.getName());

                        if (field.getType() == List.class || field.getType() == ArrayList.class && jsonObject1 instanceof  JSONArray) {
                            Object ListObject = jsonArrayToList(field, (JSONArray) jsonObject1);
                            field.set(listItem, ListObject);
                        } else {
                            if (jsonObject1 instanceof JSONObject) {
                                JSONObject customJsonObject = (JSONObject) jsonObject1;
                                Object customObject = field.getType().newInstance();
                                Field[] customObjectFields = customObject.getClass().getDeclaredFields();

                                for (Field customObjectField : customObjectFields) {
                                    customObjectField.setAccessible(true);
                                    customObjectField.set(customObject, customJsonObject.get(customObjectField.getName()));
                                }

                                field.set(customObject, customObject);

                            } else {
                                field.set(listItem, jsonObject.get(field.getName()));
                            }
                        }
                    }
                }
                arrayList.add(listItem);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return arrayList;
    }

    private static JSONArray listToJSONArray(List<Object> list) {
        JSONArray jsonArray = new JSONArray();

        for (Object object : list) {
            if (object instanceof List || object instanceof ArrayList) {
                JSONArray jsonArray1 = listToJSONArray((List<Object>) object);
                jsonArray.put(jsonArray1);
            } else {
                Field[] fields = object.getClass().getDeclaredFields();
                JSONObject jsonOject = new JSONObject();

                for (Field field : fields) {
                    field.setAccessible(true);
                    try {
                        jsonOject.put(field.getName(), field.get(object));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                jsonArray.put(jsonOject);

            }
        }

        return jsonArray;
    }

}
