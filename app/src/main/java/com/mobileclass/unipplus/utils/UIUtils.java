package com.mobileclass.unipplus.utils;

import android.app.AlertDialog;
import android.content.DialogInterface;

import androidx.appcompat.app.AppCompatActivity;

public class UIUtils {
    public static void createDialogWithMessage(AppCompatActivity appCompatActivity, String title, String message) {
        AlertDialog.Builder alertExit = new AlertDialog.Builder(appCompatActivity);
        alertExit.setTitle(title);
        alertExit.setMessage(message);
        alertExit.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertExit.show();
    }
}
