package com.mobileclass.unipplus.utils;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Locale;

public class TextUtils {

    public static String doubleToString(Double value) {
        DecimalFormat decimalFormat = new DecimalFormat("0.0");
        return decimalFormat.format(value).replace(",",".");
    }

    public static String removeAlphabetcCharacters(String text) {
        return text.replaceAll("[0-9]", "");
    }

    public static String getLocalDate() {
        final Calendar mCalendar = Calendar.getInstance();
        return String.format(
                Locale.getDefault(),
                "%d/%d/%d",
                mCalendar.get(Calendar.DAY_OF_MONTH),
                mCalendar.get(Calendar.MONTH) + 1,
                mCalendar.get(Calendar.YEAR)
        );
    }

}
