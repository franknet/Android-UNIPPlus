package com.mobileclass.unipplus.utils;

public class NumberUtils {

    public static Double decimalStringToDouble(String decimal) {
        try {
            if (decimal.trim().equalsIgnoreCase("nc")) {
                decimal = "0,0";
            }

            if (decimal.trim().equalsIgnoreCase("re")) {
                decimal = "0,0";
            }

            if (decimal.trim().equalsIgnoreCase("ap")) {
                decimal = "10,0";
            }

            return Double.parseDouble(decimal.trim().replace(".", "").replace(",", "."));
        }

        catch (NumberFormatException e) {
            return 0.0;
        }
    }

    public static int decimalStringToInt(String decimal) {
        try {
            return Integer.parseInt(decimal.trim().replace(".", "").replace(",", ""));
        }

        catch (NumberFormatException e) {
            if (decimal.trim().equalsIgnoreCase("ap")) {
                return 100;
            }

            else {
                return 0;
            }
        }
    }

    public static float mathPercentage(float percent, float value) {
        return (percent * value) / 100;
    }
}
