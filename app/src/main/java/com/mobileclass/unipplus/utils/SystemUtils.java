package com.mobileclass.unipplus.utils;

import android.util.Log;

import com.mobileclass.unipplus.configuration.Configuration;

public class SystemUtils {

    public static void infoLog(String log) {
        if (Configuration.DEVELOPMENT) {
            Log.i("UNIP Plus Info", log);
        }
    }

    public static void errorLog(String log) {
        if (Configuration.DEVELOPMENT) {
            Log.e("UNIP Plus Error", log);
        }
    }
}
