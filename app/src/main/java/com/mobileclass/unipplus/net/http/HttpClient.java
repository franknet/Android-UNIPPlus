package com.mobileclass.unipplus.net.http;

import android.os.AsyncTask;

import com.mobileclass.unipplus.UNIPPlusApplication;

import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class HttpClient extends AsyncTask<String, Integer, Void> {

    private int attempt = 0;

    public interface Result {
        void onSuccess(int statusCode, byte[] data);
        void onFailure(String message);
    }

    public enum RequestMethod {
        Get,
        Post
    }

    private byte[] mData;
    private boolean success = false;
    private String message;
    private int statusCode = 0;
    private Result result;
    private HttpProvider mProvider;

    public void result(Result result) {
        this.result = result;
    }

    public void doRequest(HttpProvider provider) {
        mProvider = provider;
    }

    // ASYNCTASK METHODS
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        CookieManager cookieManager = (CookieManager) CookieHandler.getDefault();
        if (cookieManager == null) {
            cookieManager = new CookieManager(null, CookiePolicy.ACCEPT_ALL);
            CookieHandler.setDefault(cookieManager);
        }
    }

    @Override
    protected Void doInBackground(String... strings) {
        try {
            UNIPPlusApplication.getInstance().hasInternetConnection();

            OkHttpClient client = httpClient();
            Request request = httpRequest();
            Response response = client.newCall(request).execute();

            this.statusCode = response.priorResponse() != null ? 302 : response.code();

            if (statusCode >= 200 && statusCode <= 399) {
                this.mData = response.body().bytes();
                this.success = true;
            } else {
                message = "O site da Secretaria Online encontra-se indisponível";
            }

        } catch (SocketTimeoutException e) {
            this.message = "Algo deu errado, tente novamente";
        } catch (UnknownHostException e) {
            this.message = "Site fora do ar! Verifique se o site da secretaria online está fora do ar, caso não, entre em contato com o desenvolvedor.";
        } catch (IOException e) {
            this.message = e.getMessage();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (this.success) {
            this.result.onSuccess(this.statusCode, this.mData);
            this.attempt = 0;
        } else {
            if (attempt >= 3) {
                this.result.onFailure(this.message);
            } else {
                this.execute();
                this.attempt += 1;
            }
        }
    }

    //PRIVATE METHODS
    private OkHttpClient httpClient() {
        return new OkHttpClient.Builder()
                .cookieJar(new JavaNetCookieJar(CookieHandler.getDefault()))
                .build();
    }

    private Request httpRequest() {
        Request.Builder request = new Request.Builder();
        request.url(mProvider.path());
        if (mProvider.method() == HttpClient.RequestMethod.Post) {
            request.post(mProvider.body());
        }
        return request.build();
    }
}
