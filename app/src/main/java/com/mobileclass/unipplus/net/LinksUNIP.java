package com.mobileclass.unipplus.net;

public class LinksUNIP {
    public static final String HOST = "https://sec3.unip.br";
    public static final String URL_LOGIN = "https://sistemasunip.unip.br/api-autenticacao/autenticacao/login";
    public static final String URL_HOME = "https://sec1.unip.br/?tkal=";
    public static final String URL_NOTAS_FALTAS = HOST + "/NovaSecretaria/NotasFaltasMediaFinal/NotasFaltasMediaFinal";
    public static final String URL_MEDIAS_EXAMES = HOST + "/NovaSecretaria/MediasExamesFinais/MediasExamesFinais";
    public static final String URL_HISTORICO_PAGAMENTOS = HOST + "/NovaSecretaria/PosicaoFinanceira/PosicaoFinanceira";
    public static final String URL_HISTORICO_SEMESTRAL = HOST + "/NovaSecretaria/IntegralizacaoCurricular/IntegralizacaoCurricular";
    public static final String URL_CONSULTA_ACERVO = HOST + "/ConsultaAcervoBib.aspx";
    public static final String URL_ONLINE_CLASSES = "https://sec1.unip.br/SecrOnlineNew/AulaOnline/GridAulaSemanal?Matricula=";
}