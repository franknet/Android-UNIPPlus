package com.mobileclass.unipplus.net.http;

import okhttp3.RequestBody;

public abstract class HttpProvider {
    public abstract String path();
    public abstract HttpClient.RequestMethod method();
    public abstract RequestBody body();
}
