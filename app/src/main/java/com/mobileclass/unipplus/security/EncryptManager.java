package com.mobileclass.unipplus.security;

import android.util.Base64;

import java.nio.charset.StandardCharsets;


public class EncryptManager {
    public enum EncryptCoding {
        base64
    }

    public static String encrypt(String text, EncryptCoding coding) {
        byte[] data = text.getBytes(StandardCharsets.UTF_8);

        if (coding == EncryptCoding.base64) {
            return Base64.encodeToString(data, Base64.DEFAULT);
        } else {
            return text;
        }
    }
}
