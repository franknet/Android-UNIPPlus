package com.mobileclass.unipplus;

import com.mobileclass.unipplus.modules.notasfaltas.entities.NotasFaltasEntity;

public class UNIPPlusApplicationTemp {
    private NotasFaltasEntity mNotasFaltasEntity;

    public NotasFaltasEntity getNotasFaltasEntity() {
        return mNotasFaltasEntity;
    }

    public void setNotasFaltasEntity(NotasFaltasEntity notasFaltasEntity) {
        mNotasFaltasEntity = notasFaltasEntity;
    }
}
