package com.mobileclass.unipplus.directory;

import java.io.File;
import java.io.FileOutputStream;

public class DirectoryManager {
    public static void saveFile(File file, byte[] data) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(data, 0, data.length);
            fileOutputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static File loadFile(String filePath) {
        return new File(filePath);
    }
}
