package com.mobileclass.unipplus.directory;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefDataAccess {
    private Context context;
    public static final String DATA_NOTAS_E_FALTAS = "DATA_NOTAS_E_FALTAS";
    public static final String DATA_MEDIAS_E_EXAMES = "DATA_MEDIAS_E_EXAMES";
    public static final String DATA_DADOS_CADASTRAIS = "DATA_DADOS_CADASTRAIS";
    public static final String DATA_HISTORICO_SEMESTRAL = "DATA_HISTORICO_SEMESTRAL";
    public static final String DATA_HISTORICO_PAGAMENTOS = "DATA_HISTORICO_PAGAMENTOS";
    public static final String DATA_USER = "DATA_USER";
    public static final String DATA_RATE = "DATA_RATE";
    public static final String DATA_SESSION = "DATA_SESSION";
    public static final String DATA_ANOTATIONS = "DATA_ANOTATIONS";
    public static final String DATA_CLASS_SCHEDULES = "DATA_CLASS_SCHEDULES";

    public SharedPrefDataAccess(Context context) {
        this.context = context;
    }

    private SharedPreferences getSharedPreferences() {
        String DATA_STORE = "DATA_STORE";
        return this.context.getSharedPreferences(DATA_STORE, Context.MODE_PRIVATE);
    }

    public void saveData(String saveTo, String value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(saveTo, value);
        editor.apply();
    }

    public String getData(String getFrom) {
        return getSharedPreferences().getString(getFrom, "");
    }
}
