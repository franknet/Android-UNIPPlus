package com.mobileclass.unipplus;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

public class UNIPPlusApplicationInfo {
    private Context mContext;

    UNIPPlusApplicationInfo(Context context) {
        this.mContext = context;
    }

    public long getAppVersionCode() {
        long code = 0;
        try {
            PackageInfo info = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            code = info.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return code;
    }
}
