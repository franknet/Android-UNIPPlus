package com.mobileclass.unipplus.services.google.admob;

import androidx.annotation.NonNull;

import com.google.android.gms.ads.interstitial.InterstitialAd;

public interface InterstitalAdListener {
    void onAdLoaded(@NonNull InterstitialAd interstitialAd);

    void onAdClosed();

    void onAdFailedToLoad(String error);
}
