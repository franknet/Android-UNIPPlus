package com.mobileclass.unipplus.services.google;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Map;
import java.util.Set;

public class RemoteDatabase {
    public static void saveData(Map<String, Object> data) {
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        Set<String> keys = data.keySet();
        for (String key : keys) {
            database.child(key).setValue(data.get(key));
        }
    }
}
