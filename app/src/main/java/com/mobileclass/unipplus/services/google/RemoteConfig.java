package com.mobileclass.unipplus.services.google;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigValue;

import java.util.Map;

public class RemoteConfig {
    private static RemoteConfig mInstance;
    private FirebaseRemoteConfig mRemoteConfig;

    public static RemoteConfig shared()  {
        if (mInstance == null) {
            mInstance = new RemoteConfig();
        }
        return mInstance;
    }

    public void initialize() {
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings
                .Builder()
                .setMinimumFetchIntervalInSeconds(3600)
                .build();
        mRemoteConfig = FirebaseRemoteConfig.getInstance();
        mRemoteConfig.setConfigSettingsAsync(configSettings);
        mRemoteConfig.fetchAndActivate();
    }

    public Map<String, FirebaseRemoteConfigValue> getConfiguration() {
        return mRemoteConfig.getAll();
    }

}
