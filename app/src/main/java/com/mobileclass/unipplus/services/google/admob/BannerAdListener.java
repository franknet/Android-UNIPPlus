package com.mobileclass.unipplus.services.google.admob;

import com.google.android.gms.ads.AdView;

public interface BannerAdListener {
    void onAdLoadFinished(AdView adView);
    void onAdLoadFinishedWithError(AdView adView, String message);
}
