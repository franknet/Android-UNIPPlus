package com.mobileclass.unipplus.services.google;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mobileclass.unipplus.R;

public class GoogleAnalytcsService {
    private Tracker tracker;

    public GoogleAnalytcsService(AppCompatActivity activity) {
        GoogleAnalytics googleAnalytics = GoogleAnalytics.getInstance(activity.getApplication());
        this.setTracker(googleAnalytics.newTracker(R.xml.app_tracker));
    }

    private Tracker getTracker() {
        return tracker;
    }

    private void setTracker(Tracker tracker) {
        this.tracker = tracker;
    }

    public void sendScreenName(String screenName) {
        this.getTracker().enableAutoActivityTracking(false);
        this.getTracker().setScreenName(screenName);
        this.getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    public void sendAction(String action, String category) {
        this.getTracker()
                .send(new HitBuilders.EventBuilder()
                        .setCategory(category)
                        .setAction(action)
                        .build());
    }
}
