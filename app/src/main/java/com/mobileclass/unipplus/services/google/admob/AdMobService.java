package com.mobileclass.unipplus.services.google.admob;

import android.content.Context;

import androidx.annotation.NonNull;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.mobileclass.unipplus.R;

public class AdMobService {

    public static void requestInterstitialAd(Context context, final InterstitalAdListener listener) {
        final String unitID = context.getResources().getString(R.string.unip_plus_ad);
        final AdRequest adRequest = new AdRequest.Builder().build();

        final FullScreenContentCallback contentCallback = new FullScreenContentCallback() {
            @Override
            public void onAdDismissedFullScreenContent() {
                super.onAdDismissedFullScreenContent();
                listener.onAdClosed();
            }
        };

        final InterstitialAdLoadCallback loadCallback = new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                super.onAdLoaded(interstitialAd);
                listener.onAdLoaded(interstitialAd);
                interstitialAd.setFullScreenContentCallback(contentCallback);
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                listener.onAdFailedToLoad(loadAdError.getMessage());
            }
        };

        InterstitialAd.load(context, unitID, adRequest, loadCallback);
    }

    public static void requestBannerAd(final AdView adView, final BannerAdListener listener) {
        adView.loadAd(new AdRequest.Builder().build());
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                listener.onAdLoadFinishedWithError(adView, loadAdError.getMessage());
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                listener.onAdLoadFinished(adView);
            }
        });
    }


}
